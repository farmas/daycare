namespace Daycare
{
    public class Resources
    {
        public static string AdultMalePhoto = "/images/adult-male.png";
        public static string AdultFemalePhoto = "/images/adult-female.png";
        public static string ChildMalePhoto = "/images/child-male.png";
        public static string ChildFemalePhoto = "/images/child-female.png";
        public static string UnknownUserPhoto = "/images/user-unknown.png";
    }
}