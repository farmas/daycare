import { autoinject } from 'aurelia-framework';
import { DialogController } from 'aurelia-dialog';
import { ValidationRules } from 'aurelia-validation';
import { EditableItemService, editablePart, IEditableItemPart } from '../../services/editableItemService';
import { HttpService } from '../../services/httpService';
import { Email, Parent } from '../../types';
import * as $ from 'jquery';

export interface EmailDialogModel {
    parent: Parent;
}

@autoinject
@editablePart
export class EmailDialog {
    public myDialog: Element;
    public controller: DialogController;
    public model: EmailDialogModel;
    public editable: EditableItemService;

    private _http: HttpService;

    constructor(dialogController: DialogController, editable: EditableItemService, http: HttpService) {
        this.controller = dialogController;
        this.editable = editable;
        this._http = http;
    }

    public attached(element) {
        $(this.myDialog).css("display", "initial");
    }

    public activate(model: EmailDialogModel) {
        this.model = model;
        var editablePart: IEditableItemPart<Email> = <any>this;

        editablePart.isEditOnly = true;
        editablePart.item = { id: '', displayName: '', body: '', subject: '', toUserId: model.parent.id };

        ValidationRules
            .ensure((p: Email) => p.subject).displayName('Subject').required()
            .ensure((p: Email) => p.body).displayName('Body').required()
            .on(editablePart.item);

        this.editable.init(editablePart, '/not-used');
    }

    public send() {
        var editablePart: IEditableItemPart<Email> = <any>this;

        this.editable.isIdle = false;
        this._http.request('POST', '/api/email', editablePart.item)
            .then(() => this.controller.ok())
            .catch(() => {
                this.editable.isIdle = true;
            });
    }
}