import { autoinject } from 'aurelia-framework';
import { DialogController } from 'aurelia-dialog';
import { Parent, Family } from '../../types';
import { EditableItemService } from '../../services/editableItemService';
import * as $ from 'jquery';

export interface CreateParentDialogModel {
    title: string;
    family: Family;
}

@autoinject
export class CreateParentDialog {
    public myDialog: Element;
    public controller: DialogController;
    public editable: EditableItemService;
    public model: CreateParentDialogModel;

    constructor(controller: DialogController, service: EditableItemService) {
        this.controller = controller;
        this.editable = service;
    }

    public activate(model: CreateParentDialogModel) {
        this.model = model;
    }

    public attached(element) {
        $(this.myDialog).css("display", "initial");
    }

    public inserted(item: Parent) {
        this.controller.ok(item);
    }
}