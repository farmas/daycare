import { autoinject } from 'aurelia-framework';
import { DialogController } from 'aurelia-dialog';
import { Person, Family } from '../../types';
import { EditableItemService } from '../../services/editableItemService';
import * as $ from 'jquery';

export interface CreateChildDialogModel {
    title: string;
    family: Family;
}

@autoinject
export class CreateChildDialog {
    public myDialog: Element;
    public controller: DialogController;
    public editable: EditableItemService;
    public model: CreateChildDialogModel;

    constructor(controller: DialogController, service: EditableItemService) {
        this.controller = controller;
        this.editable = service;
    }

    public activate(model: CreateChildDialogModel) {
        this.model = model;
    }

    public attached(element) {
        $(this.myDialog).css("display", "initial");
    }

    public inserted(item: Person) {
        this.controller.ok(item);
    }
}