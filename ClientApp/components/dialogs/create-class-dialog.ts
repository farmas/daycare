import { autoinject } from 'aurelia-framework';
import { DialogController } from 'aurelia-dialog';
import { Class } from '../../types';
import { EditableItemService } from '../../services/editableItemService';
import * as $ from 'jquery';

@autoinject
export class CreateClassDialog {
    public myDialog: Element;
    public controller: DialogController;
    public editable: EditableItemService;

    constructor(controller: DialogController, service: EditableItemService) {
        this.controller = controller;
        this.editable = service;
    }

    public attached(element) {
        $(this.myDialog).css("display", "initial");
    }

    public inserted(item: Class) {
        this.controller.ok(item);
    }
}