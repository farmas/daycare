import { autoinject } from 'aurelia-framework';
import { DialogController } from 'aurelia-dialog';
import { Family } from '../../types';
import * as $ from 'jquery';

@autoinject
export class CreateFamilyDialog {
    public myDialog: Element;
    public controller: DialogController;

    constructor(dialogController: DialogController) {
        this.controller = dialogController;
    }

    public attached() {
        $(this.myDialog).css("display", "initial");
    }

    public inserted(item: Family) {
        this.controller.ok(item);
    }
}