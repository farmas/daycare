import { autoinject } from 'aurelia-framework';
import { DialogController } from 'aurelia-dialog';
import { Person, Class, sortEntityList } from '../../types';
import { HttpService } from '../../services/httpService';
import * as $ from 'jquery';

export interface SelectPersonsDialogModel {
    owner: Class;
    title: string;
    role: string;
    memberPropName: string
}

@autoinject
export class SelectPersonsDialog {
    public controller: DialogController;
    public myDialog: Element;
    public availablePersons: Person[] = [];
    public availablePersonIds: string[] = [];
    public selectedPersons: Person[] = [];
    public selectedPersonIds: string[] = [];
    public model: SelectPersonsDialogModel;

    private _http: HttpService;

    constructor(controller: DialogController, http: HttpService) {
        this.controller = controller;
        this._http = http;
    }

    public activate(model: SelectPersonsDialogModel) {
        this.model = model;
        this.selectedPersons = model.owner[this.model.memberPropName].slice();
        sortEntityList(this.selectedPersons);

        this._http.get(`/api/persons?role=${this.model.role}`).then((persons: Person[]) => {
            persons.forEach(element => {
                if (!this.selectedPersons.some(m => m.id === element.id)) {
                    this.availablePersons.push(element);
                }
            });

            sortEntityList(this.availablePersons);
        });
    }

    public attached(element) {
        $(this.myDialog).css("display", "initial");
    }

    public add() {
        this._movePersons(
            this.availablePersonIds,
            this.availablePersons,
            this.selectedPersons,
            movedPerson => this._http.post(`/api/Classes/${this.model.owner.id}/${this.model.memberPropName}`, { id: movedPerson.id }));

        sortEntityList(this.selectedPersons);
    }

    public remove() {
        this._movePersons(
            this.selectedPersonIds,
            this.selectedPersons,
            this.availablePersons,
            movedPerson => this._http.delete(`/api/Classes/${this.model.owner.id}/${this.model.memberPropName}/${movedPerson.id}`));

        sortEntityList(this.availablePersons);
    }

    public done() {
        this.controller.ok(this.selectedPersons);
    }

    private _movePersons(sourcePersonIds: string[], sourceList: Person[], targetList: Person[], onPersonMoved: (person: Person) => void): void {
        sourcePersonIds.forEach(selectedPersonId => {
            var sourcePerson = sourceList.find(p => p.id === selectedPersonId);
            var selectedPersonIndex = sourceList.indexOf(sourcePerson);

            sourceList.splice(selectedPersonIndex, 1);
            targetList.push(sourcePerson);

            onPersonMoved(sourcePerson);
        });
    }
}