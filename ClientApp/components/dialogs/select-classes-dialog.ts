import { autoinject } from 'aurelia-framework';
import { DialogController } from 'aurelia-dialog';
import { Class, Person, sortEntityList } from '../../types';
import { HttpService } from '../../services/httpService';
import * as $ from 'jquery';

export interface SelectClassesDialogModel {
    owner: Person
}

@autoinject
export class SelectClassesDialog {
    public controller: DialogController;
    public myDialog: Element;
    public availableClasses: Class[] = [];
    public availableClassIds: string[] = [];
    public selectedClasses: Class[] = [];
    public selectedClassIds: string[] = [];
    public model: SelectClassesDialogModel;

    private _http: HttpService;

    constructor(controller: DialogController, http: HttpService) {
        this.controller = controller;
        this._http = http;
    }

    public activate(model: SelectClassesDialogModel) {
        this.model = model;
        this.selectedClasses = model.owner.classes.slice();

        sortEntityList(this.selectedClasses);

        this._http.get('/api/classes').then((classes: Class[]) => {
            classes.forEach(element => {
                if (!this.selectedClasses.some(m => m.id === element.id)) {
                    this.availableClasses.push(element);
                }
            });

            sortEntityList(this.availableClasses);
        });
    }

    public attached(element) {
        $(this.myDialog).css("display", "initial");
    }

    public add() {
        this._moveClasses(
            this.availableClassIds,
            this.availableClasses,
            this.selectedClasses,
            movedClass => this._http.post(`/api/Classes/${movedClass.id}/students`, { id: this.model.owner.id }));

        sortEntityList(this.selectedClasses);
    }

    public remove() {
        this._moveClasses(
            this.selectedClassIds,
            this.selectedClasses,
            this.availableClasses,
            movedClass => this._http.delete(`/api/Classes/${movedClass.id}/students/${this.model.owner.id}`));

        sortEntityList(this.availableClasses);
    }

    public done() {
        this.controller.ok(this.selectedClasses);
    }

    private _moveClasses(sourceClassIds: string[], sourceList: Class[], targetList: Class[], onClassMoved: (c: Class) => void): void {
        sourceClassIds.forEach(classId => {
            var sourceClass = sourceList.find(p => p.id === classId);
            var selectedClassIndex = sourceList.indexOf(sourceClass);

            sourceList.splice(selectedClassIndex, 1);
            targetList.push(sourceClass);

            onClassMoved(sourceClass);
        });
    }
}