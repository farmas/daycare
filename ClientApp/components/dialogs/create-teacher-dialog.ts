import { autoinject } from 'aurelia-framework';
import { DialogController } from 'aurelia-dialog';
import * as $ from 'jquery';

@autoinject
export class CreateTeacherDialog {
    public myDialog: Element;
    public controller: DialogController;

    constructor(controller: DialogController) {
        this.controller = controller;
    }

    public attached() {
        $(this.myDialog).css('display', 'initial');
    }

    public inserted(item) {
        this.controller.ok(item);
    }
}