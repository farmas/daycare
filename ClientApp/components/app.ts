import { Aurelia, PLATFORM, autoinject } from 'aurelia-framework';
import { Router, RouterConfiguration, activationStrategy } from 'aurelia-router';
import { HttpClient } from 'aurelia-fetch-client';
import { AuthService } from '../services/authService';
import * as toastr from 'toastr';

@autoinject
export class App {
    public router: Router;

    private _authService: AuthService;

    constructor(http: HttpClient, authService: AuthService) {
        this._authService = authService;

        http.configure(config => {
            config.withDefaults({
                credentials: 'same-origin',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'XsrfToken': authService.getXsrfToken()
                }
            });
            config.rejectErrorResponses();
        });

        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": false,
            "positionClass": "toast-top-full-width",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
    }

    configureRouter(config: RouterConfiguration, router: Router) {
        config.title = 'Daycare';

        config.map([
            {
                route: ['', 'home'],
                name: 'home',
                moduleId: PLATFORM.moduleName('./pages/home-page'),
                nav: false,
                title: 'Home'
            },
            {
                route: ['unathorized'],
                name: 'unathorized',
                moduleId: PLATFORM.moduleName('./pages/unauthorized-page'),
                nav: false,
                title: 'Unauthorized'
            },
            {
                route: 'myfamily',
                name: 'myfamily',
                moduleId: PLATFORM.moduleName('./pages/family-page'),
                activationStrategy: activationStrategy.replace,
                nav: !this._authService.getUserInfo().isAdmin,
                settings: { iconClass: 'fa-home' },
                title: 'My Family'
            },
            {
                route: 'families',
                name: 'families',
                moduleId: PLATFORM.moduleName('./pages/families-page'),
                nav: true,
                settings: { iconClass: 'fa-users' },
                title: 'Families'
            },
            {
                route: 'families/:id',
                name: 'family',
                moduleId: PLATFORM.moduleName('./pages/family-page'),
                activationStrategy: activationStrategy.replace,
                nav: false,
                title: 'Family'
            },
            {
                route: 'classes',
                name: 'classes',
                moduleId: PLATFORM.moduleName('./pages/classes-page'),
                nav: true,
                settings: { iconClass: 'fa-book' },
                title: 'Classes'
            },
            {
                route: 'classes/:id',
                name: 'class',
                moduleId: PLATFORM.moduleName('./pages/class-page'),
                nav: false,
                title: 'Class'
            },
            {
                route: 'teachers',
                name: 'teachers',
                moduleId: PLATFORM.moduleName('./pages/teachers-page'),
                nav: true,
                settings: { iconClass: 'fa-graduation-cap' },
                title: 'Teachers'
            }
        ]);

        this.router = router;
    }
}
