import { autoinject, bindable, containerless } from 'aurelia-framework';
import { Family } from '../../types';
import { EditableListService } from '../../services/editableListService';

@containerless
@autoinject
export class StudentsListPart {
    @bindable public family: Family;
    @bindable public showClasses: boolean;
    @bindable public readOnly: boolean = false;
    public editable: EditableListService;

    constructor(service: EditableListService) {
        this.editable = service;
    }

    public bind() {
        if (this.family) {
            this.editable.init(`/api/families/${this.family.id}/children`);
            this.editable.items = this.family.children;
        } else {
            this.editable.init('/api/persons');
            this.editable.load('role=child');
        }
    }
}