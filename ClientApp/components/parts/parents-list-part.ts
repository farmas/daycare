import { autoinject, bindable, containerless } from 'aurelia-framework';
import { Parent, Family } from '../../types';
import { EditableListService } from '../../services/editableListService';

@containerless
@autoinject
export class ParentsListPart {
    @bindable public readOnly: boolean = false;
    @bindable public family: Family;
    public editable: EditableListService;

    constructor(service: EditableListService) {
        this.editable = service;
    }

    public bind() {
        this.editable.init(`/api/families/${this.family.id}/parents`);
        this.editable.items = this.family.parents;
    }
}