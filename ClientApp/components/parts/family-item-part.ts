import { autoinject, bindable } from 'aurelia-framework';
import { ValidationRules } from 'aurelia-validation';
import { DialogService, DialogSettings } from 'aurelia-dialog';
import { EditableItemService, IEditableItemPart, editablePart } from '../../services/editableItemService';
import { HttpService } from '../../services/httpService';
import { CreateChildDialog, CreateChildDialogModel } from '../dialogs/create-child-dialog';
import { CreateParentDialog, CreateParentDialogModel } from '../dialogs/create-parent-dialog';
import { Family, Person, Parent, Class } from '../../types';
import * as $ from 'jquery';

@autoinject
@editablePart
export class FamilyItemPart {
    @bindable public familyId: string;
    @bindable public buttonText: string = 'Update';
    @bindable public readOnly: boolean = false;
    public editable: EditableItemService;
    public dialogHost: Element;
    public loaded: boolean;

    private _dialogService: DialogService;
    private _http: HttpService;

    constructor(editableService: EditableItemService, dialogService: DialogService, http: HttpService) {
        this.editable = editableService;
        this._dialogService = dialogService;
        this._http = http;
    }

    public bind() {
        var editablePart: IEditableItemPart<Family> = <any>this;
        var initEditable = () => {
            ValidationRules.ensure((c: Family) => c.displayName).displayName('Display Name')
                .required().on(editablePart.item);

            this.editable.init(editablePart, '/api/families');
            this.loaded = true;
        };

        editablePart.item = { id: '', displayName: '', children: [], parents: [] };

        if (!this.familyId) {
            initEditable();
        } else {
            this._http.get(`/api/Families/${this.familyId}?expandMembers=true&expandClasses=true`)
                .then(family => {
                    editablePart.item = family;
                    initEditable();
                });
        }
    }

    public launchCreateChildDialog() {
        var editablePart: IEditableItemPart<Family> = <any>this;
        var model: CreateChildDialogModel = {
            title: 'Add a New Child',
            family: editablePart.item
        };

        var dialogOptions: DialogSettings = {
            viewModel: CreateChildDialog,
            model: model,
            host: this.dialogHost,
            lock: false
        };

        this._dialogService.open(dialogOptions).whenClosed(response => {
            if (!response.wasCancelled) {
                editablePart.item.children.push(response.output);
            }
        });
    }

    public launchCreateParentDialog() {
        var editablePart: IEditableItemPart<Family> = <any>this;
        var model: CreateParentDialogModel = {
            title: 'Add a New Parent',
            family: editablePart.item
        };

        var dialogOptions: DialogSettings = {
            viewModel: CreateParentDialog,
            model: model,
            host: this.dialogHost,
            lock: false
        };

        this._dialogService.open(dialogOptions).whenClosed(response => {
            if (!response.wasCancelled) {
                editablePart.item.parents.push(response.output);
            }
        });
    }
}