﻿import { autoinject, bindable } from 'aurelia-framework';
import { Router } from 'aurelia-router';
import { AuthService, IUser } from '../../services/authService';

@autoinject
export class NavBar {
    public user: IUser;
    public isLocalAuth: boolean;
    @bindable public router: Router;

    constructor(authService: AuthService) {
        this.user = authService.getUserInfo();
        this.isLocalAuth = authService.getProvider() === 'local';
    }
}