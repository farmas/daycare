import { bindable } from 'aurelia-framework';

export class LoadingPart {
    @bindable public show: boolean = true;

    private _showThrottled: boolean = false;

    public bind() {
        window.setTimeout(() => this._showThrottled = true, 500);
    }

    public get showWithThrottle() {
        return this._showThrottled && this.show;
    }
}