import { autoinject, bindable } from 'aurelia-framework';
import { ValidationRules } from 'aurelia-validation';
import { DialogService, DialogSettings } from 'aurelia-dialog';
import { EmailDialog, EmailDialogModel } from '../dialogs/email-dialog';
import { Parent, Family } from '../../types';
import { EditableItemService, IEditableItemPart, editablePart } from '../../services/editableItemService';
import { AuthService } from '../../services/authService';

@autoinject
@editablePart
export class ParentItemPart {
    @bindable public readOnly: boolean = false;
    @bindable public showChildren: boolean = false;
    @bindable public showLink: boolean = false;
    @bindable public family: Family;
    @bindable public buttonText: string = "Update";
    public dialogHost: Element;
    public editable: EditableItemService;

    private _dialogService: DialogService;
    private _userId: string;

    constructor(editableService: EditableItemService, dialogService: DialogService, authService: AuthService) {
        this.editable = editableService;
        this._dialogService = dialogService;
        this._userId = authService.getUserInfo().id;
    }

    public bind() {
        var editablePart: IEditableItemPart<Parent> = <any>this;
        var emptyItem: any = { displayName: '', email: '', photoUrl: '' };
        var familyId = this.family ? this.family.id : editablePart.item.id;

        editablePart.item = editablePart.item || emptyItem;

        ValidationRules
            .ensure((p: Parent) => p.displayName).displayName('Name').required()
            .ensure((p: Parent) => p.email).displayName('Email').required().email()
            .on(editablePart.item);

        this.editable.init(editablePart, `/api/families/${familyId}/parents`);
    }

    public get allowDelete(): boolean {
        var part: IEditableItemPart<Parent> = <any>this;

        return !part.isEditOnly && part.item && part.item.id !== this._userId;
    }

    public LaunchEmailDialog() {
        var editablePart: IEditableItemPart<Parent> = <any>this;
        var model: EmailDialogModel = { parent: editablePart.item };
        var dialogSettings: DialogSettings = {
            viewModel: EmailDialog,
            model: model,
            host: this.dialogHost,
            lock: false
        };

        this._dialogService.open(dialogSettings).whenClosed(response => {
            if (!response.wasCancelled) {
                console.log('closed');
            }
        });
    }
}