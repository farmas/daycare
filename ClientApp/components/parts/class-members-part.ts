import { autoinject, bindable } from 'aurelia-framework';
import { DialogService, DialogSettings } from 'aurelia-dialog';
import { Person, Class, sortEntityList } from '../../types';
import { SelectPersonsDialog, SelectPersonsDialogModel } from '../dialogs/select-persons-dialog';
import { EditableListService } from '../../services/editableListService';

@autoinject
export class ClassMembersPart {
    @bindable public role: string = 'child';
    @bindable public item: Class;
    @bindable public readOnly: boolean = true;
    @bindable public title: string;
    public dialogHost: Element;
    public editable: EditableListService

    private _dialogService: DialogService;
    private _memberPropName: string = 'students';

    constructor(dialog: DialogService, service: EditableListService) {
        this._dialogService = dialog;
        this.editable = service;
    }

    public bind() {
        this._memberPropName = this.role === 'child' ? 'students' : 'teachers';
        this.editable.init(`/api/Classes/${this.item.id}/${this._memberPropName}`);
        this.editable.items = sortEntityList(this.members || []);
    }

    public get members(): Person[] {
        return this.item[this._memberPropName];
    }

    public set members(val) {
        this.item[this._memberPropName] = val;
    }

    public insert(person: Person): Promise<any> {
        return this.editable.insert({ id: person.id, displayName: person.displayName });
    }

    public launchDialog(): void {
        var model: SelectPersonsDialogModel = {
            owner: this.item,
            role: this.role,
            memberPropName: this._memberPropName,
            title: `Select ${this._memberPropName} for class '${this.item.displayName}'`
        };

        var dialogOptions: DialogSettings = {
            viewModel: SelectPersonsDialog,
            model: model,
            host: this.dialogHost,
            lock: false
        };

        this._dialogService.open(dialogOptions).whenClosed(response => {
            if (!response.wasCancelled) {
                this.members = response.output;
            }
        });
    }
}
