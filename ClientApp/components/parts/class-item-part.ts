﻿import { autoinject, bindable } from 'aurelia-framework';
import { ValidationRules } from 'aurelia-validation';
import { Class } from '../../types';
import { EditableItemService, IEditableItemPart, editablePart } from '../../services/editableItemService';

@autoinject
@editablePart
export class ClassItemPart {
    @bindable public buttonText: string = "Update";
    @bindable public readOnly: boolean = false;
    public editable: EditableItemService;

    constructor(service: EditableItemService) {
        this.editable = service;
    }

    public bind() {
        var editablePart: IEditableItemPart<Class> = <any>this;
        var emptyItem = { id: '', displayName: '', description: '', members: [], students: [], teachers: [] };

        editablePart.item = editablePart.item || emptyItem;

        ValidationRules.ensure((c: Class) => c.displayName).displayName('Display Name')
            .required().on(editablePart.item);

        this.editable.init(editablePart, '/api/Classes');
    }
}
