import { autoinject, bindable } from 'aurelia-framework';
import { ValidationRules } from 'aurelia-validation';
import { DialogService, DialogSettings } from 'aurelia-dialog';
import { Person, Family } from '../../types';
import { EditableItemService, IEditableItemPart, editablePart } from '../../services/editableItemService';
import { SelectClassesDialog, SelectClassesDialogModel } from '../dialogs/select-classes-dialog';
import { AuthService, IUser } from '../../services/authService';

@autoinject
@editablePart
export class StudentItemPart {
    @bindable public family: Family;
    @bindable public showClasses: boolean;
    @bindable public showParents: boolean;
    @bindable public showLink: boolean;
    @bindable public readOnly: boolean = false;
    @bindable public buttonText: string = "Update";
    public user: IUser;
    public dialogHost: Element;
    public editable: EditableItemService;

    private _dialogService: DialogService;

    constructor(service: EditableItemService, dialogService: DialogService, authService: AuthService) {
        this.editable = service;
        this.user = authService.getUserInfo();
        this._dialogService = dialogService;
    }

    public bind() {
        var editablePart: IEditableItemPart<Person> = <any>this;
        var familyId = this.family ? this.family.id : editablePart.item.id;
        var emptyItem: any = { displayName: '', age: 1, photoUrl: '' };

        editablePart.item = editablePart.item || emptyItem;

        ValidationRules
            .ensure((p: Person) => p.displayName).displayName('Name').required()
            .ensure((p: Person) => p.age).displayName('Age').required().satisfies(val => {
                var num = Number.parseInt(val);
                return Number.isInteger(num) && num > 0;
            })
            .on(editablePart.item);

        this.editable.init(editablePart, `/api/families/${familyId}/children`);
    }

    public launchSelectClassesDialog() {
        var editablePart: IEditableItemPart<Person> = <any>this;
        var model: SelectClassesDialogModel = {
            owner: editablePart.item
        };

        var dialogOptions: DialogSettings = {
            viewModel: SelectClassesDialog,
            model: model,
            host: this.dialogHost,
            lock: false
        };

        this._dialogService.open(dialogOptions).whenClosed(response => {
            if (!response.wasCancelled) {
                editablePart.item.classes = response.output;
            }
        });
    }
}