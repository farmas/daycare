import { autoinject, bindable } from 'aurelia-framework';
import { ValidationRules } from 'aurelia-validation';
import { EditableItemService, editablePart, IEditableItemPart } from '../../services/editableItemService';
import { Teacher } from '../../types';
import { AuthService } from '../../services/authService';

@editablePart
@autoinject
export class TeacherItemPart {
    @bindable public buttonText: string = "Update";
    @bindable public readOnly: boolean;
    @bindable public showBio: boolean;

    public editable: EditableItemService;
    private _userId: string;

    constructor(service: EditableItemService, authService: AuthService) {
        this.editable = service;
        this._userId = authService.getUserInfo().id;
    }

    public get allowDelete(): boolean {
        var part: IEditableItemPart<Teacher> = <any>this;
        return (!part.isEditOnly && part.item && part.item.id !== this._userId);
    }
    public bind() {
        var part: IEditableItemPart<Teacher> = <any>this;
        var emptyItem: any = { displayName: '', email: '', bio: '', role: 'teacher', photoUrl: '' };

        part.item = part.item || emptyItem;

        ValidationRules
            .ensure((p: Teacher) => p.displayName).displayName('Name').required()
            .ensure((p: Teacher) => p.email).displayName('Email').required().email()
            .on(part.item);

        this.editable.init(part, '/api/persons');
    }
}