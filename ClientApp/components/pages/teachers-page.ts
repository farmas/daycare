import { autoinject, containerless } from 'aurelia-framework';
import { AuthService, IUser } from '../../services/authService';
import { DialogService, DialogSettings } from 'aurelia-dialog';
import { Teacher, } from '../../types';
import { EditableListService } from '../../services/editableListService';
import { CreateTeacherDialog } from '../dialogs/create-teacher-dialog';

@autoinject
export class TeachersPage {
    public editable: EditableListService;
    public dialogHost: Element;
    public user: IUser;
    public loaded: boolean;

    private _dialogService: DialogService;

    constructor(authService: AuthService, service: EditableListService, dialogService: DialogService) {
        this.editable = service;
        this.user = authService.getUserInfo();
        this._dialogService = dialogService;

        this.editable.init('/api/persons');
        this.editable.load('role=teacher').then(() => this.loaded = true);
    }

    public launchCreateTeacherDialog() {
        var settings: DialogSettings = {
            viewModel: CreateTeacherDialog,
            host: this.dialogHost,
            lock: false
        };

        this._dialogService.open(settings).whenClosed(response => {
            if (!response.wasCancelled) {
                this.editable.items.push(response.output);
            }
        });
    }
}