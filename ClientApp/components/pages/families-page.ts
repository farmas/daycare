import { autoinject } from 'aurelia-framework';
import { DialogService, DialogSettings } from 'aurelia-dialog';
import { Router } from 'aurelia-router';
import { AuthService, IUser } from '../../services/authService';
import { EditableListService } from '../../services/editableListService';
import { CreateFamilyDialog } from '../dialogs/create-family-dialog';
import { Family, Entity } from '../../types';
import * as $ from 'jquery';

class GroupBy {
    public static Family = 'family';
    public static Children = 'children';
    public static Parents = 'parents';
}

@autoinject
export class FamiliesPage {
    public editable: EditableListService;
    public dialogHost: Element;
    public groupBy: string = GroupBy.Family;
    public families: Family[] = [];
    public children: Entity[] = [];
    public parents: Entity[] = [];
    public user: IUser;
    public loaded: boolean = false;

    private _dialogService: DialogService;
    private _router: Router;

    constructor(service: EditableListService, dialogService: DialogService, authService: AuthService, router: Router) {
        this._dialogService = dialogService;
        this._router = router;
        this.user = authService.getUserInfo();
        this.editable = service.init('/api/families');

        this.editable.load('expandMembers=true').then((families: Family[]) => {
            this.families = families;

            this.children = families.reduce((children, family) => {
                var members = family.children.map(child => $.extend({ parents: family.parents }, child));
                return children.concat(members);
            }, []);

            this.parents = families.reduce((parents, family) => {
                var members = family.parents.map(parent => $.extend({ children: family.children }, parent));
                return parents.concat(members);
            }, []);

            this.loaded = true;
        });
    }

    public setGroupBy(groupBy: string) {
        this.groupBy = groupBy;
    }

    public launchCreateFamilyDialog() {
        var dialogOptions: DialogSettings = {
            viewModel: CreateFamilyDialog,
            host: this.dialogHost,
            lock: false
        };

        this._dialogService.open(dialogOptions).whenClosed(response => {
            if (!response.wasCancelled) {
                var family: Family = response.output;
                this._router.navigateToRoute('family', { id: family.id });
            }
        });
    }
}