﻿import { autoinject } from 'aurelia-framework';
import { AuthService, IUser } from '../../services/authService';
import { DialogService, DialogSettings } from 'aurelia-dialog';
import { Class } from '../../types';
import { EditableListService } from '../../services/editableListService';
import { CreateClassDialog } from '../dialogs/create-class-dialog';

@autoinject
export class ClassesPage {
    public editable: EditableListService;
    public dialogHost: Element;
    public user: IUser;
    public loaded: boolean;

    private _dialogService: DialogService;

    constructor(authService: AuthService, service: EditableListService, dialogService: DialogService) {
        this.editable = service;
        this.user = authService.getUserInfo();
        this._dialogService = dialogService;

        this.editable.init('/api/Classes');
        this.editable.load('expandMembers=true').then(() => this.loaded = true);
    }

    public launchCreateClassDialog() {
        var dialogOptions: DialogSettings = {
            viewModel: CreateClassDialog,
            host: this.dialogHost,
            lock: false
        };

        this._dialogService.open(dialogOptions).whenClosed(response => {
            if (!response.wasCancelled) {
                this.editable.items.push(response.output);
            }
        });
    }
}