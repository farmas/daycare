import { autoinject } from 'aurelia-framework';
import { AuthService, IUser } from '../../services/authService';

@autoinject
export class FamilyPage {
    public user: IUser;
    public canEdit: boolean = false;
    public familyId: string;

    constructor(authService: AuthService) {
        this.user = authService.getUserInfo();
    }

    public activate(params, routeConfig) {
        this.familyId = params.id || this.user.familyId;

        this.canEdit = this.user.isAdmin || this.user.familyId === this.familyId;
    }
}