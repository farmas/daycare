﻿import { HttpService } from '../../services/httpService';
import { autoinject } from 'aurelia-framework';
import { Class } from '../../types';

@autoinject
export class ClassPage {
    private _http: HttpService;
    public class: Class;

    constructor(http: HttpService) {
        this._http = http;
    }

    activate(params, routeConfig) {
        return this._http.get(`/api/Classes/${params.id}?expandMembers=true`)
            .then(data => {
                this.class = data;
            });
    }
}
