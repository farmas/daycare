import { autoinject, transient } from 'aurelia-framework';
import { EditableResourceService, ResolveUrlFunc } from './editableResourceService';
import { Entity } from '../types';

@autoinject
@transient()
export class EditableListService {
    private _resourceService: EditableResourceService;

    public items: Entity[];

    constructor(resourceService: EditableResourceService) {
        this._resourceService = resourceService;
    }

    public init(url: string): EditableListService {
        this._resourceService.init(url);
        return this;
    }

    public load(query?: string): Promise<Entity[]> {
        return this._resourceService.get(query).then(data => {
            this.items = data;
            return data;
        });
    }

    public insert(item: Entity): Promise<Entity> {
        return this._resourceService.insert(item).then(data => {
            this.inserted(data);
            return data;
        });
    }

    public inserted(item: Entity) {
        this.items.push(item);
    }

    public update(item: Entity): Promise<Entity> {
        return this._resourceService.update(item);
    }

    public delete(item: Entity): Promise<any> {
        if (window.confirm(`Are you sure you want to delete '${item.displayName}'?`)) {
            return this._resourceService.delete(item).then(() => {
                this.deleted(item);
            });
        }
    }

    public deleted(item: Entity): void {
        var index = this.items.findIndex(i => i.id === item.id);
        this.items.splice(index, 1);
    }
}
