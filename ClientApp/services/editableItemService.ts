import { autoinject, bindable, transient } from 'aurelia-framework';
import { ValidationController, Validator, ValidationControllerFactory, validateTrigger } from 'aurelia-validation';
import { EditableResourceService, ResolveUrlFunc } from './editableResourceService';
import { Entity } from '../types';
import * as $ from 'jquery';

export function editablePart<T extends Function>(target: T) {
    bindable({
        name: 'isEditOnly'
    })(target);

    bindable({
        name: 'onDeleted'
    })(target);

    bindable({
        name: 'onInserted'
    })(target);

    bindable({
        name: 'onUpdated'
    })(target);

    bindable({
        name: 'onCanceled'
    })(target);

    bindable({
        name: 'item'
    })(target);
}

export interface IEditableItemPart<T extends Entity> {
    isEditOnly: boolean;
    onUpdated: ({ item: T }) => void;
    onInserted: ({ item: T }) => void;
    onDeleted: ({ item: T }) => void;
    onCanceled: () => void;
    item: T;
}

@autoinject
@transient()
export class EditableItemService {
    public isEditMode: boolean;
    public isIdle: boolean = true;
    public canSave: boolean = false;

    private _savedItem: Entity;
    private _part: IEditableItemPart<Entity>;
    private _validationController: ValidationController;
    private _validator: Validator;
    private _resource: EditableResourceService;

    constructor(validator: Validator, validatorFactory: ValidationControllerFactory, resource: EditableResourceService) {
        this._resource = resource;
        this._validator = validator;
        this._validationController = validatorFactory.createForCurrentScope(validator);
        this._validationController.validateTrigger = validateTrigger.blur;
    }

    public init(part: IEditableItemPart<Entity>, url: string) {
        this._part = part;
        this._resource.init(url);

        let validate = () => {
            this._validator.validateObject(this._part.item).then(results => {
                this.canSave = results.every(result => result.valid);
            });
        };

        this.isEditMode = this._part.isEditOnly;

        this._validationController.subscribe(event => validate());
        validate();
    }

    public enterEditMode(): void {
        this._savedItem = $.extend({}, this._part.item);
        this.isEditMode = true || this._part.isEditOnly;
    }

    public cancelEditMode(): void {
        $.extend(this._part.item, this._savedItem);
        this.isEditMode = false || this._part.isEditOnly;
        this._part.onCanceled && this._part.onCanceled();
    }

    public insert(): Promise<any> {
        this.isIdle = false;

        return this._resource.insert(this._part.item)
            .then(data => {
                this.clearItem();
                this.isEditMode = false || this._part.isEditOnly;
                this.isIdle = true;
                this._part.onInserted && this._part.onInserted({ item: data });
            })
            .catch(() => {
                this.isIdle = true;
            });
    }

    public update(): void {
        this.isIdle = false;

        this._resource.update(this._part.item)
            .then(data => {
                $.extend(this._part.item, data);
                this.isEditMode = false || this._part.isEditOnly;
                this.isIdle = true;
                this._part.onUpdated && this._part.onUpdated({ item: this._part.item });
            })
            .catch(() => {
                this.isIdle = true;
            });
    }

    public save(): void {
        if (this._part.isEditOnly) {
            this.insert();
        } else {
            this.update();
        }
    }

    public delete(): void {
        if (window.confirm(`Are you sure you want to delete '${this._part.item.displayName}'?`)) {
            this.isIdle = false;

            this._resource.delete(this._part.item)
                .then(() => this._part.onDeleted && this._part.onDeleted({ item: this._part.item }))
                .catch(() => this.isIdle = true);
        }
    }

    private clearItem() {
        Object.getOwnPropertyNames(this._part.item).forEach(prop => {
            if (!prop.startsWith('_')) {
                this._part.item[prop] = Array.isArray(this._part.item[prop]) ? [] : '';
            }
        });
    }
}