import { autoinject, transient } from 'aurelia-framework';
import { HttpService } from './httpService';
import { Entity } from '../types';

export interface ResolveUrlFunc {
    (item?: Entity): string;
}

@autoinject
@transient()
export class EditableResourceService {
    private _http: HttpService;
    private _url: string;

    constructor(http: HttpService) {
        this._http = http;
    }

    public init(url: string): EditableResourceService {
        this._url = url;
        return this;
    }

    public get(query?: string): Promise<Entity[]> {
        var url = this._url + (query ? `?${query}` : '');
        return this._http.get(url);
    }

    public insert(item: Entity): Promise<Entity> {
        return this._http.post(this._url, item);
    }

    public update(item: Entity): Promise<Entity> {
        var url = this._url + '/' + item.id;
        return this._http.put(url, item);
    }

    public delete(item: Entity): Promise<any> {
        var url = this._url + '/' + item.id;
        return this._http.delete(url);
    }
}
