﻿import { HttpClient } from 'aurelia-fetch-client';
import { autoinject } from 'aurelia-framework';
import * as toastr from 'toastr';

@autoinject
export class HttpService {
    private _http: HttpClient;

    constructor(http: HttpClient) {
        this._http = http;
    }

    public get(url: string): Promise<any> {
        return this.requestJson('GET', url);
    }

    public post(url: string, body: any): Promise<any> {
        return this.requestJson('POST', url, body);
    }

    public put(url: string, body: any): Promise<any> {
        return this.requestJson('PUT', url, body);
    }

    public delete(url: string): Promise<any> {
        return this.request('DELETE', url);
    }

    public requestJson(method: string, url: string, body: any = null): Promise<any> {
        var requestObj = { method: method.toLocaleUpperCase() };

        if (body) {
            requestObj['body'] = JSON.stringify(body)
        }

        return new Promise((resolve, reject) => {
            this._http.fetch(url, requestObj)
                .then(result => result.json())
                .then(data => resolve(data))
                .catch((reason: Response) => this._handleError(reason, reject));
        });
    }

    public request(method: string, url: string, body: any = null): Promise<Response> {
        var requestObj = { method: method.toLocaleUpperCase() };

        if (body) {
            requestObj['body'] = JSON.stringify(body)
        }

        return new Promise((resolve, reject) => {
            this._http.fetch(url, requestObj)
                .then(result => resolve(result))
                .catch((reason: Response) => this._handleError(reason, reject));
        });
    }

    private _handleError(reason: Response, reject: (reason?: any) => void) {
        if (reason.status === 401) {
            console.log('Unauthorized');
            toastr.error('Unauthorized');
            reject('Unathorized')
        } else if (reason.status === 500) {
            console.log('Unknown Error');
            toastr.error('Unknown Error');
            reject('Unknown Error');
        } else if (reason.status === 400) {
            console.log('Bad Request');
            toastr.error('Bad Request');
            reject('Bad Request');
        } else if (reason.json) {
            reason.json().then(reasonJson => {
                console.log('Error', reasonJson);
                toastr.error('Error:' + JSON.stringify(reasonJson));
                reject(reasonJson);
            }).catch((error) => {
                console.log('Error', error);
                toastr.error('Error:' + error);
                reject(error);
            });
        } else {
            var reasonStr = reason.toString();
            console.log('Error', reasonStr);
            toastr.error('Error: ' + reasonStr);
            reject(reasonStr);
        }
    }
}