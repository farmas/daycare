﻿export interface IUser {
    id: string;
    isAuthenticated: boolean;
    isAdmin: boolean;
    displayName: string;
    familyId: string;
    role: string;
}

export class AuthService {
    private _user: IUser;

    public getUserInfo(): IUser {
        if (!this._user) {
            var userDisplayName = document.getElementById('authUserDisplayName').innerText.trim();
            var userFamilyId = document.getElementById('authUserFamilyId').innerText.trim();
            var userRole = document.getElementById('authUserRole').innerText.trim();
            var userId = document.getElementById('authUserId').innerText.trim();

            this._user = {
                id: userId,
                displayName: userDisplayName,
                familyId: userFamilyId,
                isAuthenticated: !!userDisplayName,
                role: userRole,
                isAdmin: userRole === 'admin' || userRole === 'teacher'
            }
        }

        return this._user;
    }

    public getProvider(): string {
        var authProvider = document.getElementById('authProvider').innerText.trim();
        return authProvider;
    }

    public getXsrfToken(): string {
        var elements = document.getElementsByName("__RequestVerificationToken");

        if (elements && elements.length > 0) {
            var val = elements[0].getAttribute('value');
            if (val) {
                return val.trim();
            }
        }

        return null;
    }
}