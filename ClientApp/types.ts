﻿export interface Entity {
    id: string;
    displayName: string;
}

export interface Family extends Entity {
    children: Person[];
    parents: Person[];
}

export interface Person extends Entity {
    role: string;
    photoUrl: string;
    age: number;
    classes: Class[];
}

export interface Teacher extends Entity {
    role: string;
    photoUrl: string;
    email: string;
    bio: string;
}

export interface Parent extends Entity {
    role: string;
    photoUrl: string;
    email: string;
}

export interface Class extends Entity {
    description: string;
    students: Person[];
    teachers: Teacher[];
}

export interface Email extends Entity {
    subject: string;
    body: string;
    toUserId: string;
}

export function sortEntityList(entities: Entity[]): Entity[] {
    entities.sort((a, b) => {
        if (a.displayName < b.displayName)
            return -1;
        if (a.displayName > b.displayName)
            return 1;
        return 0;
    });

    return entities;
}