﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Daycare.DataModels;

namespace Daycare.Services
{
    public class MockFamilyDataService : IFamilyDataService
    {
        public Task DeleteFamilyAsync(string familyId)
        {
            InMemoryDatabase.Families.TryRemove(familyId, out Family family);
            return Task.CompletedTask;
        }

        public Task<IEnumerable<Family>> GetFamiliesAsync()
        {
            return Task.FromResult<IEnumerable<Family>>(InMemoryDatabase.Families.Values);
        }

        public Task<Family> GetFamilyAsync(string familyId)
        {
            InMemoryDatabase.Families.TryGetValue(familyId, out Family family);
            return Task.FromResult(family);
        }

        public Task<Family> InsertFamilyAsync(Family family)
        {
            family.Id = Guid.NewGuid().ToString();
            var newFamily = InMemoryDatabase.Families.AddOrUpdate(family.Id, family, (k, f) => family);
            return Task.FromResult(newFamily);
        }

        public Task<Family> UpdateFamilyAsync(Family family)
        {
            var updatedFamily = InMemoryDatabase.Families.AddOrUpdate(family.Id, family, (id, fam) => family);
            return Task.FromResult(updatedFamily);
        }
    }
}
