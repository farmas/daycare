﻿using Daycare.DataModels;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Daycare.Services
{
    public static class InMemoryDatabase
    {
        private class PersonInfo
        {
            public string Name { get; set; }
            public string Image { get; set; }
            public string[] Classes { get; set; }

            public PersonInfo(string name, string image)
                : this(name, image, new string[0])
            {
            }

            public PersonInfo(string name, string image, params string[] classes)
            {
                this.Name = name;
                this.Image = image;
                this.Classes = classes;
            }
        }

        public static ConcurrentDictionary<string, Person> Persons = new ConcurrentDictionary<string, Person>();
        public static ConcurrentDictionary<string, Family> Families = new ConcurrentDictionary<string, Family>();
        public static ConcurrentDictionary<string, Class> Classes = new ConcurrentDictionary<string, Class>();

        static InMemoryDatabase()
        {
            /*classes */
            var classPotions = new Class() { Id = "class1", DisplayName = "Potions", Description = "In this class, students learn the correct way to brew potions." };
            var classTransfiguration = new Class() { Id = "class2", DisplayName = "Transfiguration", Description = "In this class, students learn the correct way to brew potions." };
            var classDAD = new Class() { Id = "class3", DisplayName = "Defence Against the Dark Arts", Description = "In this class students learn how to magically defend themselves against Dark Creatures, the Dark Arts, and other dark charms." };
            var classHerbology = new Class() { Id = "class4", DisplayName = "Herbology", Description = "Herbology is the study of magical and mundane plants and fungi, making it the wizarding equivalent to botany." };

            Classes.TryAdd(classPotions.Id, classPotions);
            Classes.TryAdd(classTransfiguration.Id, classTransfiguration);
            Classes.TryAdd(classDAD.Id, classDAD);
            Classes.TryAdd(classHerbology.Id, classHerbology);

            /*professors */
            CreateTeacher(new PersonInfo("Severus Snape", "http://res.cloudinary.com/farmas/image/upload/v1511980565/hogwarts/snape.jpg", classPotions.Id));
            CreateTeacher(new PersonInfo("Remus Lupin", "http://res.cloudinary.com/farmas/image/upload/v1511980564/hogwarts/lupin.jpg", classDAD.Id));
            CreateTeacher(new PersonInfo("Pomona Sprout", "http://res.cloudinary.com/farmas/image/upload/v1511980566/hogwarts/sprout.png", classHerbology.Id));
            CreateTeacher(new PersonInfo("Minerva McGonagall", "http://res.cloudinary.com/farmas/image/upload/v1511980564/hogwarts/mcgonagall.jpg", classTransfiguration.Id));

            /*families */
            CreateFamily("Potter",
                new List<PersonInfo>() {
                    new PersonInfo("James Potter", Resources.UnknownUserPhoto),
                    new PersonInfo("Lilly Potter", Resources.UnknownUserPhoto)},
                new List<PersonInfo>() {
                    new PersonInfo("Harry Potter", "http://res.cloudinary.com/farmas/image/upload/v1511980563/hogwarts/harrypotter.jpg", classPotions.Id, classTransfiguration.Id )});

            CreateFamily("Weasley",
                new List<PersonInfo>() {
                    new PersonInfo("Arthur Weasley", "http://res.cloudinary.com/farmas/image/upload/v1511980566/hogwarts/arthur.jpg"),
                    new PersonInfo("Molly Prewett", "http://res.cloudinary.com/farmas/image/upload/v1511980565/hogwarts/MOLLY1.jpg")},
                new List<PersonInfo>() {
                    new PersonInfo("William Weasley", "http://res.cloudinary.com/farmas/image/upload/v1511980563/hogwarts/Bill-Weasley.jpg"),
                    new PersonInfo("Charles Weasley", Resources.UnknownUserPhoto),
                    new PersonInfo("Percy Weasley", "http://res.cloudinary.com/farmas/image/upload/v1511980564/hogwarts/Percy.jpg"),
                    new PersonInfo("Fred Weasley", "http://res.cloudinary.com/farmas/image/upload/v1511980564/hogwarts/Fred.jpg"),
                    new PersonInfo("George Weasley", "http://res.cloudinary.com/farmas/image/upload/v1511980563/hogwarts/george.jpg"),
                    new PersonInfo("Ronald Weasley", "http://res.cloudinary.com/farmas/image/upload/v1511980565/hogwarts/Ron_Weasley.jpg", classDAD.Id, classPotions.Id, classTransfiguration.Id),
                    new PersonInfo("Ginny Weasley", "http://res.cloudinary.com/farmas/image/upload/v1511980565/hogwarts/Ginny.jpg")});

            CreateFamily("Granger",
                    new List<PersonInfo>() {
                        new PersonInfo("Mr. Granger", Resources.UnknownUserPhoto),
                        new PersonInfo("Ms. Granger", Resources.UnknownUserPhoto)},
                    new List<PersonInfo>() {
                        new PersonInfo("Hermione Granger", "http://res.cloudinary.com/farmas/image/upload/v1511980563/hogwarts/hermione.jpg", classHerbology.Id, classDAD.Id, classPotions.Id)});
        }

        private static void CreateTeacher(PersonInfo teacher)
        {
            var userId = Guid.NewGuid().ToString();
            InMemoryDatabase.Persons.TryAdd(userId, new Person()
            {
                Id = userId,
                DisplayName = teacher.Name,
                Email = "parent1@family.com",
                PhotoUrl = teacher.Image,
                Role = PersonRoles.Teacher,
                ClassIds = teacher.Classes.ToList()
            });
        }

        private static void CreateFamily(string familyName, IEnumerable<PersonInfo> parents, IEnumerable<PersonInfo> children)
        {
            var familyKey = Guid.NewGuid().ToString();
            var myFamily = InMemoryDatabase.Families.AddOrUpdate(
                familyKey,
                key => new Family()
                {
                    Id = familyKey,
                    DisplayName = familyName
                },
                (key, family) => family);

            foreach (var parent in parents)
            {
                var userId = Guid.NewGuid().ToString();
                InMemoryDatabase.Persons.TryAdd(userId, new Person()
                {
                    Id = userId,
                    FamilyId = familyKey,
                    DisplayName = parent.Name,
                    Email = "parent1@family.com",
                    PhotoUrl = parent.Image,
                    Role = PersonRoles.Parent
                });
            }

            foreach (var child in children)
            {
                var userId = Guid.NewGuid().ToString();
                InMemoryDatabase.Persons.TryAdd(userId, new Person()
                {
                    Id = userId,
                    FamilyId = familyKey,
                    DisplayName = child.Name,
                    Email = null,
                    Age = 5,
                    PhotoUrl = child.Image,
                    Role = PersonRoles.Child,
                    ClassIds = child.Classes.ToList()
                });
            }
        }

        public static Person EnsureFamilyExists(string familyName, string parent1Name, string parent2Name, string child1Name, string child2Name)
        {
            var userId = Guid.NewGuid().ToString();

            var familyKey = userId + "_family";
            var myFamily = InMemoryDatabase.Families.AddOrUpdate(
                familyKey,
                key => new Family()
                {
                    Id = familyKey,
                    DisplayName = familyName
                },
                (key, family) => family);

            var firstParent = InMemoryDatabase.Persons.GetOrAdd(userId, key => new Person()
            {
                Id = userId,
                FamilyId = familyKey,
                DisplayName = parent1Name,
                Email = "parent1@family.com",
                PhotoUrl = Resources.AdultMalePhoto,
                Role = PersonRoles.Parent
            });

            var secondParentKey = userId + "_parent2";
            var secondParent = InMemoryDatabase.Persons.AddOrUpdate(
                secondParentKey,
                key => new Person()
                {
                    Id = secondParentKey,
                    FamilyId = familyKey,
                    DisplayName = parent2Name,
                    Email = "parent2@family.com",
                    PhotoUrl = Resources.AdultFemalePhoto,
                    Role = PersonRoles.Parent
                }, (key, person) => person);

            var firstChildKey = userId + "_child1";
            var firstChild = InMemoryDatabase.Persons.AddOrUpdate(
                firstChildKey,
                key => new Person()
                {
                    Id = firstChildKey,
                    FamilyId = familyKey,
                    DisplayName = child1Name,
                    Email = firstChildKey,
                    Age = 5,
                    ClassIds = new List<string>() { Classes.First().Key },
                    PhotoUrl = Resources.ChildMalePhoto,
                    Role = PersonRoles.Child
                }, (key, person) => person);

            var secondChildKey = userId + "_child2";
            var secondChild = InMemoryDatabase.Persons.AddOrUpdate(
                secondChildKey,
                key => new Person()
                {
                    Id = secondChildKey,
                    FamilyId = familyKey,
                    DisplayName = child2Name,
                    Email = secondChildKey,
                    Age = 3,
                    ClassIds = new List<string>() { Classes.Skip(1).First().Key },
                    PhotoUrl = Resources.ChildFemalePhoto,
                    Role = PersonRoles.Child
                }, (key, person) => person);

            return firstParent;
        }
    }
}
