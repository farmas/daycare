namespace Daycare.Services
{
    public class Email
    {
        public string Body { get; set; }
        public string Subject { get; set; }
        public string ToAddress { get; set; }
        public string ToDisplayName { get; set; }
        public string FromAddress { get; set; }
        public string FromDisplayName { get; set; }
    }
}