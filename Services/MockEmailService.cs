using System;
using System.Threading.Tasks;

namespace Daycare.Services
{
    public class MockEmailService : IEmailService
    {
        public Task SendEmailAsync(Email email)
        {
            Console.WriteLine("Email Sent");
            return Task.CompletedTask;
        }
    }
}