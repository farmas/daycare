using System;
using Daycare.DataModels;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using MongoDB.Driver.Linq;

namespace Daycare.Services
{
    public class MongoDatabaseService
    {
        private readonly MongoClient _client;
        private readonly IMongoDatabase _db;

        public MongoDatabaseService(IConfiguration configuration)
        {
            var mongoUrl = configuration["database:mongodb:url"];
            var dbName = configuration["database:mongodb:dbname"] ?? "daycare";
            _client = new MongoClient(mongoUrl);
            _db = _client.GetDatabase(dbName);
        }

        public IMongoCollection<Family> Families
        {
            get
            {
                return _db.GetCollection<Family>("families");
            }
        }

        public IMongoCollection<Person> Persons
        {
            get
            {
                return _db.GetCollection<Person>("persons");
            }
        }

        public IMongoCollection<Class> Classes
        {
            get
            {
                return _db.GetCollection<Class>("classes");
            }
        }
    }
}