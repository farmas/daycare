﻿using Daycare.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Daycare.Services
{
    public interface IFamilyDataService
    {
        Task<IEnumerable<Family>> GetFamiliesAsync();
        Task<Family> GetFamilyAsync(string familyId);
        Task<Family> UpdateFamilyAsync(Family family);
        Task<Family> InsertFamilyAsync(Family family);
        Task DeleteFamilyAsync(string familyId);
    }
}
