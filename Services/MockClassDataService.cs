﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Daycare.DataModels;

namespace Daycare.Services
{
    public class MockClassDataService : IClassDataService
    {
        public Task DeleteClassAsync(string classId)
        {
            InMemoryDatabase.Classes.TryRemove(classId, out Class theClass);
            return Task.FromResult(true);
        }

        public Task<Class> GetClassAsync(string classId)
        {
            return Task.FromResult(InMemoryDatabase.Classes[classId]);
        }

        public Task<IEnumerable<Class>> GetClassesAsync(Expression<Func<Class, bool>> predicate)
        {
            var classes = InMemoryDatabase.Classes.Values.Where(predicate.Compile());
            return Task.FromResult(classes);
        }

        public Task<Class> InsertClassAsync(Class theClass)
        {
            theClass.Id = Guid.NewGuid().ToString();
            return UpdateClassAsync(theClass);
        }

        public Task<Class> UpdateClassAsync(Class theClass)
        {
            var result = InMemoryDatabase.Classes.AddOrUpdate(theClass.Id, theClass, (id, oldClass) => theClass);
            return Task.FromResult(result);
        }
    }
}
