using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Daycare.DataModels;
using MongoDB.Driver;

namespace Daycare.Services
{
    public class MongoFamilyDataService : IFamilyDataService
    {
        private readonly MongoDatabaseService _db;

        public MongoFamilyDataService(MongoDatabaseService databaseService)
        {
            _db = databaseService;
        }
        public Task DeleteFamilyAsync(string familyId)
        {
            return _db.Families.DeleteOneAsync(Builders<Family>.Filter.Eq(f => f.Id, familyId));
        }

        public async Task<IEnumerable<Family>> GetFamiliesAsync()
        {
            return await _db.Families.Find(_ => true).ToListAsync();
        }

        public Task<Family> GetFamilyAsync(string familyId)
        {
            return _db.Families.Find(Builders<Family>.Filter.Eq(f => f.Id, familyId)).FirstAsync();
        }

        public async Task<Family> InsertFamilyAsync(Family family)
        {
            family.Id = MongoDB.Bson.ObjectId.GenerateNewId().ToString();
            await _db.Families.InsertOneAsync(family);
            return family;
        }

        public async Task<Family> UpdateFamilyAsync(Family family)
        {
            await _db.Families.ReplaceOneAsync(Builders<Family>.Filter.Eq(f => f.Id, family.Id), family);
            return family;
        }
    }
}