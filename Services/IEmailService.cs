using System;
using System.Threading.Tasks;

namespace Daycare.Services
{
    public interface IEmailService
    {
        Task SendEmailAsync(Email email);
    }
}