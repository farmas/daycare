﻿using Daycare.DataModels;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using System.Linq.Expressions;

namespace Daycare.Services
{
    public interface IClassDataService
    {
        Task<Class> GetClassAsync(string classId);
        Task<IEnumerable<Class>> GetClassesAsync(Expression<Func<Class, bool>> predicate);
        Task<Class> UpdateClassAsync(Class theClass);
        Task<Class> InsertClassAsync(Class theClass);
        Task DeleteClassAsync(string classId);
    }
}
