﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Daycare.DataModels;

namespace Daycare.Services
{
    public class MockPersonDataService : IPersonDataService
    {
        public Task AddClassAsync(string personId, string classId)
        {
            InMemoryDatabase.Persons.TryGetValue(personId, out Person person);
            person.ClassIds.Add(classId);
            InMemoryDatabase.Persons.TryUpdate(personId, person, person);
            return Task.CompletedTask;
        }

        public Task<Person> AddPersonAsync(Person person)
        {
            person.Id = Guid.NewGuid().ToString();
            if (String.IsNullOrWhiteSpace(person.FamilyId))
            {
                person.FamilyId = Guid.NewGuid().ToString();
            }

            var newPerson = InMemoryDatabase.Persons.AddOrUpdate(person.Id, person, (key, p) => person);
            return Task.FromResult(newPerson);
        }

        public Task DeletePersonAsync(string personId)
        {
            InMemoryDatabase.Persons.TryRemove(personId, out Person person);
            return Task.FromResult(person);
        }

        public Task<IEnumerable<Person>> GetPersonsAsync(Expression<Func<Person, bool>> predicate)
        {
            var persons = InMemoryDatabase.Persons.Values.Where(predicate.Compile());
            return Task.FromResult(persons);
        }

        public Task RemoveClassAsync(string personId, string classId)
        {
            InMemoryDatabase.Persons.TryGetValue(personId, out Person person);
            person.ClassIds.Remove(classId);
            InMemoryDatabase.Persons.TryUpdate(personId, person, person);
            return Task.CompletedTask;
        }

        public Task<Person> SeedPersonDataAsync(string displayName, string email)
        {
            var user = InMemoryDatabase.EnsureFamilyExists(
                $"[{displayName}]", displayName, "Parent 2", "Child 1", "Child 2");

            return Task.FromResult(user);
        }

        public Task<Person> UpdatePersonAsync(Person person)
        {
            if (InMemoryDatabase.Persons.TryGetValue(person.Id, out Person dbPerson))
            {
                // Do not allow updates to modify the id or the familyId.
                dbPerson.Age = person.Age;
                dbPerson.Bio = person.Bio;
                dbPerson.DisplayName = person.DisplayName;
                dbPerson.Email = person.Email;
                dbPerson.PhotoUrl = person.PhotoUrl;

                InMemoryDatabase.Persons.TryUpdate(dbPerson.Id, dbPerson, dbPerson);

                return Task.FromResult(dbPerson);
            }

            throw new InvalidOperationException($"Person with id '{person.Id}' was not found.");
        }
    }
}
