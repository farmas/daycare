﻿using Daycare.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace Daycare.Services
{
    public interface IPersonDataService
    {
        Task<Person> SeedPersonDataAsync(string displayName, string email);
        Task<IEnumerable<Person>> GetPersonsAsync(Expression<Func<Person, bool>> predicate);
        Task<Person> UpdatePersonAsync(Person person);
        Task AddClassAsync(string personId, string classId);
        Task RemoveClassAsync(string personId, string classId);
        Task DeletePersonAsync(string personId);
        Task<Person> AddPersonAsync(Person person);
    }
}
