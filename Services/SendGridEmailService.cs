using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace Daycare.Services
{
    public class SendGridEmailService : IEmailService
    {
        private readonly IConfiguration _configuration;

        public SendGridEmailService(IConfiguration configuration)
        {
            this._configuration = configuration;
        }

        public async Task SendEmailAsync(Email email)
        {
            var apiKey = _configuration["mail:sendgrid:apikey"];
            var client = new SendGridClient(apiKey);
            var from = new EmailAddress(email.FromAddress, email.FromDisplayName);
            var to = new EmailAddress(email.ToAddress, email.ToDisplayName);
            var msg = MailHelper.CreateSingleEmail(
                from,
                to, email.Subject,
                email.Body,
                email.Body);

            var response = await client.SendEmailAsync(msg);

            if (response.StatusCode != HttpStatusCode.Accepted)
            {
                var body = await response.Body.ReadAsStringAsync();
                throw new InvalidOperationException($"Failed to send email. Body: {body}");
            }
        }
    }
}