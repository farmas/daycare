using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Daycare.DataModels;
using MongoDB.Driver;
using MongoDB.Driver.Linq;

namespace Daycare.Services
{
    public class MongoClassDataService : IClassDataService
    {
        private readonly MongoDatabaseService _db;

        public MongoClassDataService(MongoDatabaseService databaseService)
        {
            _db = databaseService;
        }

        public Task DeleteClassAsync(string classId)
        {
            return _db.Classes.DeleteOneAsync(Builders<Class>.Filter.Eq(c => c.Id, classId));
        }

        public Task<Class> GetClassAsync(string classId)
        {
            return _db.Classes.Find(Builders<Class>.Filter.Eq(c => c.Id, classId)).FirstAsync();
        }

        public async Task<IEnumerable<Class>> GetClassesAsync(Expression<Func<Class, bool>> predicate)
        {
            return await _db.Classes.AsQueryable().Where(predicate).ToListAsync();
        }

        public async Task<Class> InsertClassAsync(Class theClass)
        {
            theClass.Id = MongoDB.Bson.ObjectId.GenerateNewId().ToString();
            await _db.Classes.InsertOneAsync(theClass);
            return theClass;
        }

        public async Task<Class> UpdateClassAsync(Class theClass)
        {
            await _db.Classes.ReplaceOneAsync(Builders<Class>.Filter.Eq(c => c.Id, theClass.Id), theClass);
            return theClass;
        }
    }
}