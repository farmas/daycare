using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Daycare.DataModels;
using MongoDB.Driver;
using MongoDB.Driver.Linq;

namespace Daycare.Services
{
    public class MongoPersonDataService : IPersonDataService
    {
        private readonly MongoDatabaseService _db;

        public MongoPersonDataService(MongoDatabaseService databaseService)
        {
            _db = databaseService;
        }

        public Task AddClassAsync(string personId, string classId)
        {
            var filter = Builders<Person>.Filter.Eq(p => p.Id, personId);
            var update = Builders<Person>.Update.AddToSet(p => p.ClassIds, classId);
            return _db.Persons.UpdateOneAsync(filter, update);
        }

        public async Task<Person> AddPersonAsync(Person person)
        {
            person.Id = MongoDB.Bson.ObjectId.GenerateNewId().ToString();

            if (String.IsNullOrWhiteSpace(person.FamilyId))
            {
                person.FamilyId = MongoDB.Bson.ObjectId.GenerateNewId().ToString();
            }

            await _db.Persons.InsertOneAsync(person);
            return person;
        }

        public Task DeletePersonAsync(string personId)
        {
            var filter = Builders<Person>.Filter.Eq(p => p.Id, personId);
            return _db.Persons.DeleteOneAsync(filter);
        }

        public async Task<IEnumerable<Person>> GetPersonsAsync(Expression<Func<Person, bool>> predicate)
        {
            return await _db.Persons.AsQueryable().Where(predicate).ToListAsync();
        }

        public Task RemoveClassAsync(string personId, string classId)
        {
            var filter = Builders<Person>.Filter.Eq(p => p.Id, personId);
            var update = Builders<Person>.Update.Pull(p => p.ClassIds, classId);
            return _db.Persons.UpdateOneAsync(filter, update);
        }

        public async Task<Person> SeedPersonDataAsync(string displayName, string email)
        {
            var family = new Family()
            {
                Id = MongoDB.Bson.ObjectId.GenerateNewId().ToString(),
                DisplayName = "Family of " + displayName
            };
            await _db.Families.InsertOneAsync(family);

            var person = new Person()
            {
                Id = MongoDB.Bson.ObjectId.GenerateNewId().ToString(),
                FamilyId = family.Id,
                DisplayName = displayName,
                Role = PersonRoles.Parent,
                Email = email,
                PhotoUrl = Resources.UnknownUserPhoto
            };
            await _db.Persons.InsertOneAsync(person);

            await _db.Persons.InsertOneAsync(new Person()
            {
                Id = MongoDB.Bson.ObjectId.GenerateNewId().ToString(),
                DisplayName = "Maestra Claudia",
                Role = PersonRoles.Teacher,
                PhotoUrl = Resources.AdultFemalePhoto
            });

            await _db.Classes.InsertOneAsync(new Class()
            {
                Id = MongoDB.Bson.ObjectId.GenerateNewId().ToString(),
                DisplayName = "Morning Class",
                Description = "8am - 12pm Class"
            });

            return person;
        }

        public Task<Person> UpdatePersonAsync(Person person)
        {
            var filter = Builders<Person>.Filter.Eq(p => p.Id, person.Id);
            var update = Builders<Person>.Update
                .Set(p => p.Age, person.Age)
                .Set(p => p.Bio, person.Bio)
                .Set(p => p.DisplayName, person.DisplayName)
                .Set(p => p.Email, person.Email)
                .Set(p => p.PhotoUrl, person.PhotoUrl);
            var options = new FindOneAndUpdateOptions<Person>
            {
                ReturnDocument = ReturnDocument.After
            };

            return _db.Persons.FindOneAndUpdateAsync(filter, update, options);
        }
    }
}