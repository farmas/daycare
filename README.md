# Day Care#

Web Application that models a daycare or small school, including classes, teachers, students, their families. Features:

- Integrated authentication with Google.
- School administrators can create students and assign them to classes.
- Parents can see other students enrolled in their child's class.
- Parents can contact parents of other students via email through the application (email addresses are not revealed).

Sample application is available here: http://daycare-sample.azurewebsites.net

## Create/Run production image ##
- Run 'docker-compose build' to build the production image.
- Run 'docker-compose up' to launch production container.

## Setup for local development ##

### Requirements ###
- Node.js 6 or above
- ASP.NET Core SDK 2.0.0
- MongoDB 3.4 or above

### Install Dependencies ###
- 'npm install' from root directory.
- 'dotnet restore' from root directory.

### Setup environment ###
- (First Run Only) Run 'npm run setup' from root directory.
- Set ASPNETCORE_ENVIRONMENT environment variable to 'Development'
    - If you’re using PowerShell in Windows, execute $Env:ASPNETCORE_ENVIRONMENT = "Development"
    - If you’re using cmd.exe in Windows, execute setx ASPNETCORE_ENVIRONMENT "Development", and then restart your command prompt to make the change take effect
    - If you’re using Mac/Linux, execute export ASPNETCORE_ENVIRONMENT=Development

### Run the project ###
- 'dotnet run' from root directory
- Navigate to http://localhost:5000

## Setup Docker container for development ##
- Install and run docker on your local machine.
- Run 'npm install' from root directory.
- Run 'npm run setup' from root directory.
- Run 'docker-compose -f .\docker-compose-dev.yml build' to build the development image.
- Run 'docker-compose -f .\docker-compose-dev.yml up' to launch the container with your dev folder mounted as a volume.
    - Docker may prompt you to share your disk for the container.
- Navigate to 'http://localhost:5000' to load the application.
- Notes:
    - Container is setup with a .NET file watcher that will re-launch the .NET process when any C# file changes.
    - Container is setup with webpack in dev server mode.
    - Container is setup to poll for front-end changes and re-run webpack (Hot-module replacement is not enabled, after making a change you will have to reload the browser).

## Setup Docker container + Mongo container for development ##
- Install and run docker on your local machine.
- Run 'npm install' from root directory.
- Run 'npm run setup' from root directory.
- Run 'docker-compose -f .\docker-compose-dev-mongo.yml build'
- Run 'docker-compose -f .\docker-compose-dev-mongo.yml up'
- Navigate to 'http://localhost:5000' to load the application.
- Notes:
    - This will run the same development container that is setup in the previous section but configured to run with MongoDB.
    - Additionally it will run a MongoDB service and link both containers.

## Setup Google Authentication ##
- Gather your client id and client secret from the [google developer console](https://console.developers.google.com/apis/credentials?project=daycare-171416).
    - Set 'auth:scheme' to 'Google' on appsettings.json.
    - Set 'auth:adminemail' to your google email on appsettings.json.
    - Run 'dotnet user-secrets set auth:google:clientid <YOUR_CLIENT_ID>'.
    - Run 'dotnet user-secrets set auth:google:clientsecret <YOUR_CLIENT_SECRET>'.

## Setup SendGrid for E-Mail delivery ##
- Gather your api key from the [SendGrid Dashboard](https://app.sendgrid.com/).
    - Set 'mail:provider' to 'SendGrid' in appsettings.json.
    - Run 'dotnet user-secrets set mail:sendgrid:apikey <YOUR_API_KEY>'.

## Setup MongoDB ##
- Gather the mongodb connection string from your provider. If you are using the default local server, you may leave the url setting as is.
    - Set 'database:provider' to 'MongoDB' in appsettings.json
    - Set 'database:mongodb:dbname' to the database name in appsettings.json.
    - Run 'dotnet user-secrets set database:mongodb:url <YOUR_CONNECTION_STRING>'.