# build image
FROM microsoft/aspnetcore-build:2.0.3 as build
WORKDIR /app

COPY *.csproj ./
COPY NuGet.config ./
RUN dotnet restore

COPY . ./
RUN dotnet publish -c Release -o out

# runtime image
FROM microsoft/aspnetcore:2.0.3
ENV PORT=5000
WORKDIR /app
COPY --from=build /app/out .
CMD ASPNETCORE_URLS=http://*:$PORT dotnet Daycare.dll