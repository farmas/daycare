﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson.Serialization.Attributes;

namespace Daycare.DataModels
{
    public class Person
    {
        [BsonId]
        public string Id { get; set; }
        public string FamilyId { get; set; }
        public string DisplayName { get; set; }
        public string Email { get; set; }
        public string Bio { get; set; }
        public string Role { get; set; }
        public string PhotoUrl { get; set; }
        public int Age { get; set; }
        public List<string> ClassIds { get; set; } = new List<string>();
    }
}
