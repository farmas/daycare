﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson.Serialization.Attributes;

namespace Daycare.DataModels
{
    public class Class
    {
        [BsonId]
        public string Id { get; set; }
        public string DisplayName { get; set; }
        public string Description { get; set; }
    }
}
