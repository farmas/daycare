﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson.Serialization.Attributes;

namespace Daycare.DataModels
{
    public class Family
    {
        [BsonId]
        public string Id { get; set; }
        public string DisplayName { get; set; }
    }
}
