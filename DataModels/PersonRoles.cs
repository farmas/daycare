﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Daycare.DataModels
{
    public static class PersonRoles
    {
        public const string Admin = "admin";
        public const string Teacher = "teacher";
        public const string Parent = "parent";
        public const string Child = "child";
    }
}
