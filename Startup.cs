using Daycare.Services;
using Daycare.Controllers.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.SpaServices.Webpack;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.Google;

namespace Daycare
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add app services
            this.AddAuthenticationServices(services);
            this.AddDatabaseServices(services);
            this.AddEmailServices(services);

            // Add configuration
            services.AddSingleton<IConfiguration>(this.Configuration);

            // Add framework services.
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddMvc();
            services.AddAntiforgery(options =>
            {
                options.HeaderName = "XsrfToken";
                options.SuppressXFrameOptionsHeader = true;
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();

                var webpackOptions = new WebpackDevMiddlewareOptions
                {
                    HotModuleReplacement = true
                };

                var dockerEnvironment = Environment.GetEnvironmentVariable("DOCKER_ENVIRONMENT");
                if (!String.IsNullOrEmpty(dockerEnvironment))
                {
                    webpackOptions.EnvironmentVariables = new Dictionary<string, string>() { { "DOCKER_ENVIRONMENT", dockerEnvironment } };
                }

                app.UseWebpackDevMiddleware(webpackOptions);
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();
            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");

                routes.MapSpaFallbackRoute(
                    name: "spa-fallback",
                    defaults: new { controller = "Home", action = "Index" });
            });
        }

        private void AddEmailServices(IServiceCollection services)
        {
            if (IsSendGrid())
            {
                services.AddSingleton<IEmailService, SendGridEmailService>();
            }
            else
            {
                services.AddSingleton<IEmailService, MockEmailService>();
            }
        }

        private void AddDatabaseServices(IServiceCollection services)
        {
            if (IsMongoDB())
            {
                services.AddScoped<MongoDatabaseService>();
                services.AddScoped<IPersonDataService, MongoPersonDataService>();
                services.AddScoped<IFamilyDataService, MongoFamilyDataService>();
                services.AddScoped<IClassDataService, MongoClassDataService>();
            }
            else
            {
                services.AddSingleton<IPersonDataService, MockPersonDataService>();
                services.AddSingleton<IFamilyDataService, MockFamilyDataService>();
                services.AddSingleton<IClassDataService, MockClassDataService>();
            }
        }

        private void AddAuthenticationServices(IServiceCollection services)
        {
            var authBuilder = services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(options =>
                {
                    options.Events = new CookieAuthenticationEvents()
                    {
                        OnRedirectToAccessDenied = ctx =>
                        {
                            ctx.Response.StatusCode = 401;
                            return Task.FromResult<object>(null);
                        }
                    };
                });

            if (IsGoogleAuthentication())
            {
                authBuilder.AddGoogle(options =>
                {
                    options.ClientId = Configuration["auth:google:clientid"];
                    options.ClientSecret = Configuration["auth:google:clientsecret"];
                });
            }

            services.AddAuthorization(options =>
            {
                options.AddPolicy(AuthorizationPolicies.FamilyOrAdmin, policy =>
                    policy.Requirements.Add(new FamilyOrRolesAuthorizationRequirement(AuthorizationRoles.Admin, AuthorizationRoles.Teacher)));
            });

            services.AddSingleton<IAuthorizationHandler, FamilyOrRolesAuthorizationHandler>();
        }

        private bool IsGoogleAuthentication()
        {
            return String.Equals(Configuration["auth:scheme"], GoogleDefaults.AuthenticationScheme, StringComparison.OrdinalIgnoreCase);
        }

        private bool IsMongoDB()
        {
            return String.Equals(Configuration["database:provider"], "MongoDb", StringComparison.OrdinalIgnoreCase);
        }

        private bool IsSendGrid()
        {
            return String.Equals(Configuration["mail:provider"], "SendGrid", StringComparison.OrdinalIgnoreCase);
        }
    }
}
