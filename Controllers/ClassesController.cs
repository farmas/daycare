﻿using Daycare.Controllers.RequestModels;
using Daycare.Controllers.ResponseModels;
using Daycare.DataModels;
using Daycare.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daycare.Controllers.Authentication;

namespace Daycare.Controllers
{
    [Authorize]
    [AutoValidateAntiforgeryToken]
    [Route("api/[controller]")]
    public class ClassesController : Controller
    {
        private readonly IClassDataService _classService;
        private readonly IPersonDataService _personService;

        public ClassesController(IClassDataService classService, IPersonDataService personService)
        {
            _classService = classService;
            _personService = personService;
        }

        [HttpGet]
        public async Task<IEnumerable<ClassResponseModel>> GetClasses(bool expandMembers = false)
        {
            var classesTask = _classService.GetClassesAsync(_ => true);
            var personsTask = expandMembers ? _personService.GetPersonsAsync(_ => true) : Task.FromResult(Enumerable.Empty<Person>());

            await Task.WhenAll(classesTask, personsTask);

            var classModels = classesTask.Result.Select(theClass =>
            {
                var classModel = ClassResponseModel.FromDataModel(theClass);
                var user = new AuthUser(this.User.Claims);
                if (expandMembers)
                {
                    classModel.Students = personsTask.Result
                        .Where(p => p.Role == PersonRoles.Child && p.ClassIds.Contains(classModel.Id))
                        .Select(p => PersonResponseModel.FromDataModel(p, user));

                    classModel.Teachers = personsTask.Result
                        .Where(p => p.Role == PersonRoles.Teacher && p.ClassIds.Contains(classModel.Id))
                        .Select(p => PersonResponseModel.FromDataModel(p, user));
                }
                return classModel;
            });

            return classModels;
        }

        [HttpGet("{id}")]
        public async Task<ClassResponseModel> GetClass(string id, bool expandMembers = false)
        {
            var classTask = _classService.GetClassAsync(id);
            var personsTask = expandMembers ? _personService.GetPersonsAsync(p => p.ClassIds.Contains(id)) : Task.FromResult(Enumerable.Empty<Person>());
            var user = new AuthUser(this.User.Claims);

            await Task.WhenAll(classTask, personsTask);

            var classModel = ClassResponseModel.FromDataModel(classTask.Result);
            if (expandMembers)
            {
                classModel.Students = personsTask.Result.Where(p => p.Role == PersonRoles.Child).Select(p => PersonResponseModel.FromDataModel(p, user));
                classModel.Teachers = personsTask.Result.Where(p => p.Role == PersonRoles.Teacher).Select(p => PersonResponseModel.FromDataModel(p, user));
            }

            return classModel;
        }

        [HttpPut("{id}")]
        [AuthorizeRoles(AuthorizationRoles.Admin, AuthorizationRoles.Teacher)]
        public async Task<IActionResult> UpdateClass(string id, [FromBody] ClassUpdateRequestModel requestModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var updatedClass = await _classService.UpdateClassAsync(requestModel.ToDataModel());
            return Ok(ClassResponseModel.FromDataModel(updatedClass));
        }

        [HttpPost]
        [AuthorizeRoles(AuthorizationRoles.Admin, AuthorizationRoles.Teacher)]
        public async Task<IActionResult> InsertClass([FromBody] ClassInsertRequestModel requestModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var newClass = await _classService.InsertClassAsync(requestModel.ToDataModel());
            var responseModel = ClassResponseModel.FromDataModel(newClass);
            responseModel.Students = Enumerable.Empty<PersonResponseModel>();
            responseModel.Teachers = Enumerable.Empty<PersonResponseModel>();
            return Ok(responseModel);
        }

        [HttpDelete("{id}")]
        [AuthorizeRoles(AuthorizationRoles.Admin, AuthorizationRoles.Teacher)]
        public async Task<IActionResult> DeleteClass(string id)
        {
            await _classService.DeleteClassAsync(id);
            return Ok();
        }

        [HttpDelete("{classId}/students/{studentId}")]
        [AuthorizeRoles(AuthorizationRoles.Admin, AuthorizationRoles.Teacher)]
        public async Task<IActionResult> RemoveStudent(string classId, string studentId)
        {
            await _personService.RemoveClassAsync(studentId, classId);
            return Ok();
        }

        [HttpPost("{classId}/students")]
        [AuthorizeRoles(AuthorizationRoles.Admin, AuthorizationRoles.Teacher)]
        public Task<IActionResult> AddStudent(string classId, [FromBody] EntityRequestModel student)
        {
            return AddPersonAsync(classId, student);
        }

        [HttpDelete("{classId}/teachers/{teacherId}")]
        [AuthorizeRoles(AuthorizationRoles.Admin, AuthorizationRoles.Teacher)]
        public async Task<IActionResult> RemoveTeacher(string classId, string teacherId)
        {
            await _personService.RemoveClassAsync(teacherId, classId);
            return Ok();
        }

        [HttpPost("{classId}/teachers")]
        [AuthorizeRoles(AuthorizationRoles.Admin, AuthorizationRoles.Teacher)]
        public Task<IActionResult> AddTeacher(string classId, [FromBody] EntityRequestModel teacher)
        {
            return AddPersonAsync(classId, teacher);
        }

        private async Task<IActionResult> AddPersonAsync(string classId, EntityRequestModel person)
        {
            await _personService.AddClassAsync(person.Id, classId);
            var user = new AuthUser(this.User.Claims);
            var persons = await _personService.GetPersonsAsync(p => p.Id == person.Id);

            return Ok(PersonResponseModel.FromDataModel(persons.First(), user));
        }
    }
}
