using Daycare.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using Daycare.DataModels;
using Daycare.Controllers.ResponseModels;
using Daycare.Controllers.Authentication;
using Daycare.Controllers.RequestModels;

namespace Daycare.Controllers
{
    [Authorize]
    [AutoValidateAntiforgeryToken]
    [Route("api/[controller]")]
    public class PersonsController : Controller
    {
        private readonly IPersonDataService _personService;

        public PersonsController(IPersonDataService personService)
        {
            this._personService = personService;
        }

        [HttpGet]
        public async Task<IActionResult> GetPersons(string role = null)
        {
            var user = new AuthUser(this.User.Claims);
            var persons = await _personService.GetPersonsAsync(p => String.IsNullOrEmpty(role) || p.Role == role);
            var students = persons.Select(p => PersonResponseModel.FromDataModel(p, user));

            return Ok(students);
        }

        [HttpPost]
        [AuthorizeRoles(AuthorizationRoles.Admin, AuthorizationRoles.Teacher)]
        public async Task<IActionResult> InsertPerson([FromBody] PersonInsertRequestModel requestModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var user = new AuthUser(this.User.Claims);
            var dataModel = requestModel.ToDataModel();
            var newPerson = await _personService.AddPersonAsync(dataModel);
            var responseModel = PersonResponseModel.FromDataModel(newPerson, user);
            return Ok(responseModel);
        }

        [HttpDelete("{personId}")]
        [AuthorizeRoles(AuthorizationRoles.Admin, AuthorizationRoles.Teacher)]
        public async Task<IActionResult> DeletePerson(string personId)
        {
            await _personService.DeletePersonAsync(personId);
            return Ok();
        }

        [HttpPut("{personId}")]
        [AuthorizeRoles(AuthorizationRoles.Admin, AuthorizationRoles.Teacher)]
        public async Task<IActionResult> UpdatePerson([FromBody] PersonUpdateRequestModel requestModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = new AuthUser(this.User.Claims);
            var updatedPerson = await _personService.UpdatePersonAsync(requestModel.ToDataModel());
            return Ok(PersonResponseModel.FromDataModel(updatedPerson, user));
        }
    }
}