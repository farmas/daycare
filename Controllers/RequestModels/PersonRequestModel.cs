using Daycare.DataModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daycare.Controllers.RequestModels
{
    public class PersonInsertRequestModel
    {
        [Required(AllowEmptyStrings = false)]
        public string DisplayName { get; set; }

        public string FamilyId { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public string Role { get; set; }

        public int Age { get; set; }

        public string PhotoUrl { get; set; } = Resources.UnknownUserPhoto;

        public string Bio { get; set; }

        public virtual Person ToDataModel()
        {
            return new Person()
            {
                DisplayName = this.DisplayName,
                PhotoUrl = String.IsNullOrWhiteSpace(this.PhotoUrl) ? Resources.UnknownUserPhoto : this.PhotoUrl,
                Role = this.Role,
                Bio = this.Bio,
                Age = this.Age,
                Email = this.Email
            };
        }
    }

    public class PersonUpdateRequestModel : PersonInsertRequestModel
    {
        [Required(AllowEmptyStrings = false)]
        public string Id { get; set; }

        public override Person ToDataModel()
        {
            var person = base.ToDataModel();
            person.Id = this.Id;
            return person;
        }
    }

    public class ChildInsertRequestModel
    {
        [Required(AllowEmptyStrings = false)]
        public string DisplayName { get; set; }

        [Required]
        public int Age { get; set; }

        public string PhotoUrl { get; set; } = Resources.UnknownUserPhoto;

        public virtual Person ToDataModel()
        {
            return new Person()
            {
                DisplayName = this.DisplayName,
                Age = this.Age,
                PhotoUrl = String.IsNullOrWhiteSpace(this.PhotoUrl) ? Resources.UnknownUserPhoto : this.PhotoUrl,
                Role = PersonRoles.Child
            };
        }
    }

    public class ChildUpdateRequestModel : ChildInsertRequestModel
    {
        [Required(AllowEmptyStrings = false)]
        public string Id { get; set; }

        public override Person ToDataModel()
        {
            var person = base.ToDataModel();
            person.Id = this.Id;
            return person;
        }
    }

    public class ParentInsertRequestModel
    {
        [Required(AllowEmptyStrings = false)]
        public string DisplayName { get; set; }

        [Required]
        public string Email { get; set; }

        public string PhotoUrl { get; set; } = Resources.UnknownUserPhoto;

        public virtual Person ToDataModel()
        {
            return new Person()
            {
                DisplayName = this.DisplayName,
                PhotoUrl = String.IsNullOrWhiteSpace(this.PhotoUrl) ? Resources.UnknownUserPhoto : this.PhotoUrl,
                Role = PersonRoles.Parent,
                Email = this.Email
            };
        }
    }

    public class ParentUpdateRequestModel : ParentInsertRequestModel
    {
        [Required(AllowEmptyStrings = false)]
        public string Id { get; set; }

        public override Person ToDataModel()
        {
            var parent = base.ToDataModel();
            parent.Id = this.Id;
            return parent;
        }
    }
}
