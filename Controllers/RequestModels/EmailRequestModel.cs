using System.ComponentModel.DataAnnotations;

namespace Daycare.Controllers.RequestModels
{
    public class EmailRequestModel
    {
        [Required(AllowEmptyStrings = false)]
        public string Subject { get; set; }

        [Required(AllowEmptyStrings = false)]
        public string Body { get; set; }

        [Required(AllowEmptyStrings = false)]
        public string ToUserId { get; set; }
    }
}