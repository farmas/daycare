﻿using Daycare.DataModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daycare.Controllers.RequestModels
{
    public class ClassUpdateRequestModel
    {
        [Required(AllowEmptyStrings = false)]
        public string Id { get; set; }
        [Required(AllowEmptyStrings = false)]
        public string DisplayName { get; set; }
        public string Description { get; set; }

        public Class ToDataModel()
        {
            return new Class()
            {
                Id = this.Id,
                DisplayName = this.DisplayName,
                Description = this.Description
            };
        }
    }

    public class ClassInsertRequestModel
    {
        [Required(AllowEmptyStrings = false)]
        public string DisplayName { get; set; }
        public string Description { get; set; }

        public Class ToDataModel()
        {
            return new Class()
            {
                DisplayName = this.DisplayName,
                Description = this.Description
            };
        }
    }
}
