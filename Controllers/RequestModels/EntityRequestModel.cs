using Daycare.DataModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daycare.Controllers.RequestModels
{
    public class EntityRequestModel
    {
        [Required(AllowEmptyStrings = false)]
        public string Id { get; set; }
    }
}