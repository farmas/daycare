using Daycare.DataModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daycare.Controllers.RequestModels
{
    public class FamilyInsertRequestModel
    {
        [Required(AllowEmptyStrings = false)]
        public string DisplayName { get; set; }

        public Family ToDataModel()
        {
            return new Family()
            {
                DisplayName = this.DisplayName
            };
        }
    }

    public class FamilyUpdateRequestModel
    {
        [Required(AllowEmptyStrings = false)]
        public string Id { get; set; }

        [Required(AllowEmptyStrings = false)]
        public string DisplayName { get; set; }

        public Family ToDataModel()
        {
            return new Family()
            {
                Id = this.Id,
                DisplayName = this.DisplayName
            };
        }
    }
}