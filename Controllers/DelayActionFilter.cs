using System.Threading;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Daycare.Controllers
{
    public class DelayActionFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            Thread.Sleep(5000);
        }
    }
}