using Daycare.Services;
using Daycare.Controllers.Authentication;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using Microsoft.AspNetCore.Antiforgery;
using Microsoft.Extensions.Configuration;

namespace Daycare.Controllers
{
    public class HomeController : Controller
    {
        private readonly IConfiguration _config;

        public HomeController(IConfiguration config)
        {
            this._config = config;
        }
        public IActionResult Index()
        {
            if (this.User.Identity.IsAuthenticated)
            {
                var userInfo = new AuthUser(this.User.Claims);
                ViewData["authUserDisplayName"] = userInfo.DisplayName;
                ViewData["authUserFamilyId"] = userInfo.FamilyId;
                ViewData["authUserId"] = userInfo.Id;
                ViewData["authUserRole"] = userInfo.Role;
            }

            var redirected = TempData["redirected"] as bool?;
            if (!redirected.HasValue)
            {
                ViewData["showSplashScreen"] = true;
            }

            ViewData["authProvider"] = this._config["auth:scheme"].ToLowerInvariant();

            return View("Index");
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
