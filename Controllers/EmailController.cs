using System;
using System.Linq;
using System.Threading.Tasks;
using Daycare.Controllers.Authentication;
using Daycare.Controllers.RequestModels;
using Daycare.DataModels;
using Daycare.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Daycare.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class EmailController : Controller
    {
        private readonly IEmailService _emailService;
        private readonly IPersonDataService _personService;
        private readonly ILogger _logger;

        public EmailController(IEmailService emailService, IPersonDataService personService, ILogger<EmailController> logger)
        {
            _emailService = emailService;
            _personService = personService;
            _logger = logger;
        }

        [HttpPost]
        public async Task<IActionResult> SendEmail([FromBody] EmailRequestModel requestModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = new AuthUser(this.User.Claims);
            var persons = await _personService.GetPersonsAsync(p => p.Id == user.Id || p.Id == requestModel.ToUserId);
            var personsMap = persons.ToDictionary(p => p.Id);

            if (personsMap.TryGetValue(user.Id, out Person fromPerson) && personsMap.TryGetValue(requestModel.ToUserId, out Person toPerson))
            {
                var email = new Email()
                {
                    Subject = requestModel.Subject,
                    Body = requestModel.Body,
                    FromAddress = fromPerson.Email,
                    FromDisplayName = fromPerson.DisplayName,
                    ToAddress = toPerson.Email,
                    ToDisplayName = toPerson.DisplayName
                };

                _logger.LogInformation($@"
                    Sending Mail.
                    To: {email.ToAddress}.
                    From: {email.FromAddress}.
                    Subject: {email.Subject}.
                    Body: {email.Body}.");

                try
                {
                    await _emailService.SendEmailAsync(email);
                    return Ok();
                }
                catch (Exception ex)
                {
                    return BadRequest($"Error sending email: {ex.Message}");
                }
            }
            else
            {
                return BadRequest("Invalid Users");
            }
        }
    }
}