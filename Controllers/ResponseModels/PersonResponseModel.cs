﻿using Daycare.DataModels;
using Daycare.Controllers.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Daycare.Controllers.ResponseModels
{
    public class PersonResponseModel
    {
        public string Id { get; set; }
        public string DisplayName { get; set; }
        public string Role { get; set; }
        public string PhotoUrl { get; set; }
        public int Age { get; set; }
        public string FamilyId { get; set; }

        public string Bio { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Email { get; set; }

        [Newtonsoft.Json.JsonIgnore]
        public IEnumerable<string> ClassIds { get; set; } = Enumerable.Empty<string>();

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public IEnumerable<ClassResponseModel> Classes { get; set; }

        private PersonResponseModel()
        {
        }

        public static PersonResponseModel FromDataModel(Person person, AuthUser user)
        {
            var responseModel = new PersonResponseModel()
            {
                Id = person.Id,
                DisplayName = person.DisplayName,
                Role = person.Role,
                PhotoUrl = person.PhotoUrl,
                Age = person.Age,
                FamilyId = person.FamilyId,
                Bio = person.Bio,
                ClassIds = new List<string>(person.ClassIds)
            };

            if (String.Equals(user.Role, AuthorizationRoles.Admin)
               || String.Equals(user.Role, AuthorizationRoles.Teacher)
               || String.Equals(user.FamilyId, person.FamilyId))
            {
                // Do not include emails of people on payloads, except for admins/ teachers
                //   and your own family.
                responseModel.Email = person.Email;
            }

            return responseModel;
        }
    }
}
