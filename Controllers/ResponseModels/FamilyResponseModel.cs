﻿using Daycare.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Daycare.Controllers.Authentication;

namespace Daycare.Controllers.ResponseModels
{
    public class FamilyResponseModel
    {
        public string Id { get; set; }
        public string DisplayName { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public IEnumerable<PersonResponseModel> Children { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public IEnumerable<PersonResponseModel> Parents { get; set; }

        public static FamilyResponseModel FromDataModel(Family family)
        {
            return new FamilyResponseModel()
            {
                Id = family.Id,
                DisplayName = family.DisplayName
            };
        }

        public void LoadMembers(IEnumerable<Person> persons, AuthUser user)
        {
            Func<string, IEnumerable<PersonResponseModel>> getFamilyMembersResponseModels = (string role) =>
            {
                var members = persons.Where(m => String.Equals(m.Role, role, StringComparison.OrdinalIgnoreCase) && String.Equals(m.FamilyId, this.Id));
                return members.Select(m => PersonResponseModel.FromDataModel(m, user));
            };

            this.Children = getFamilyMembersResponseModels(PersonRoles.Child);
            this.Parents = getFamilyMembersResponseModels(PersonRoles.Parent);
        }
    }
}
