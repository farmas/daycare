﻿using Daycare.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Daycare.Controllers.ResponseModels
{
    public class ClassResponseModel
    {
        public string Id { get; set; }
        public string DisplayName { get; set; }
        public string Description { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public IEnumerable<PersonResponseModel> Students { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public IEnumerable<PersonResponseModel> Teachers { get; set; }

        public static ClassResponseModel FromDataModel(Class classModel)
        {
            return new ClassResponseModel()
            {
                Id = classModel.Id,
                DisplayName = classModel.DisplayName,
                Description = classModel.Description
            };
        }
    }
}
