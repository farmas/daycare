using Microsoft.AspNetCore.Authorization;

namespace Daycare.Controllers.Authentication
{
    public class FamilyOrRolesAuthorizationRequirement : IAuthorizationRequirement
    {
        public string[] Roles { get; private set; }

        public FamilyOrRolesAuthorizationRequirement(params string[] roles)
        {
            this.Roles = roles;
        }
    }
}