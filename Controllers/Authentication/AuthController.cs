﻿using Daycare.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.Google;
using Daycare.DataModels;

namespace Daycare.Controllers.Authentication
{
    public class AuthController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly IPersonDataService _personDataService;

        public AuthController(IConfiguration configuration, IPersonDataService personDataService)
        {
            _configuration = configuration;
            _personDataService = personDataService;
        }

        public Task<IActionResult> SignIn()
        {
            if (String.Equals(_configuration["auth:scheme"], GoogleDefaults.AuthenticationScheme, StringComparison.OrdinalIgnoreCase))
            {
                return SignInWithGoogleAsync();
            }

            return SignInWithLocalIdentityAsync();
        }

        private bool SupportSeedUserData()
        {
            return GetConfigBoolean("auth:seeduserdata");
        }

        private bool GetConfigBoolean(string setting)
        {
            var result = (bool.TryParse(_configuration[setting], out bool seed) && seed);
            return result;
        }

        private async Task<IActionResult> SignInWithLocalIdentityAsync()
        {
            var adminEmail = _configuration["auth:adminemail"];
            var role = _configuration["auth:local:userrole"];
            var person = (await _personDataService.GetPersonsAsync(p => p.Email == adminEmail)).FirstOrDefault();
            var seedUserData = SupportSeedUserData();

            if (person == null)
            {
                if (seedUserData)
                {
                    person = await _personDataService.SeedPersonDataAsync($"Test User [{role}]", adminEmail);
                }
                else
                {
                    person = new Person()
                    {
                        Id = Guid.NewGuid().ToString(),
                        FamilyId = Guid.NewGuid().ToString(),
                        DisplayName = $"Test User [{role}]",
                        Email = adminEmail
                    };
                }
            }

            var appUserInfo = new AuthUser(
                person.Id,
                person.FamilyId,
                person.DisplayName,
                person.Email,
                role);

            await HttpContext.SignInAsync(appUserInfo.Principal);

            TempData["redirected"] = true;
            return Redirect(Url.Action("Index", "Home"));
        }

        private Task<IActionResult> SignInWithGoogleAsync()
        {
            var authenticationProperties = new AuthenticationProperties
            {
                RedirectUri = Url.Action("SignInWithGoogleCallback")
            };

            return Task.FromResult<IActionResult>(Challenge(authenticationProperties, GoogleDefaults.AuthenticationScheme));
        }

        [Authorize]
        public async Task<IActionResult> SignInWithGoogleCallback()
        {
            var adminEmail = _configuration["auth:adminemail"];
            var googleUserInfo = new AuthUser(HttpContext.User.Claims);
            var person = (await _personDataService.GetPersonsAsync(p => p.Email == googleUserInfo.Email)).FirstOrDefault();
            var redirectUrl = Url.Action("Index", "Home");
            var isAdmin = String.Equals(adminEmail, googleUserInfo.Email, StringComparison.OrdinalIgnoreCase);
            var seedUserData = SupportSeedUserData();
            var validateUser = GetConfigBoolean("auth:google:validateuser");
            var unknownUserRole = _configuration["auth:google:unknownuserrole"];

            if (person == null)
            {
                if (seedUserData)
                {
                    person = await _personDataService.SeedPersonDataAsync(googleUserInfo.DisplayName, googleUserInfo.Email);
                }
                else if (isAdmin || !validateUser)
                {
                    person = new Person()
                    {
                        Id = googleUserInfo.Id,
                        FamilyId = Guid.NewGuid().ToString(),
                        DisplayName = googleUserInfo.DisplayName,
                        Email = googleUserInfo.Email
                    };
                }

                person.Role = isAdmin ? PersonRoles.Admin : unknownUserRole;
            }

            await HttpContext.SignOutAsync();

            if (person == null)
            {
                redirectUrl += "#unathorized";
            }
            else
            {
                var appUserInfo = new AuthUser(
                    person.Id,
                    person.FamilyId,
                    person.DisplayName,
                    person.Email,
                    person.Role);

                if (person.Role == AuthorizationRoles.User)
                {
                    // If this is a user, automatically redirect him/her to the family page.
                    redirectUrl += "#myfamily";
                }

                await HttpContext.SignInAsync(appUserInfo.Principal);
            }

            TempData["redirected"] = true;
            return Redirect(redirectUrl);
        }

        public async Task<IActionResult> SignOut()
        {
            await HttpContext.SignOutAsync();

            TempData["redirected"] = true;
            return RedirectToAction("Index", "Home");
        }
    }
}
