using System.Threading.Tasks;
using System.Linq;
using Daycare.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using System;

namespace Daycare.Controllers.Authentication
{
    public class FamilyOrRolesAuthorizationHandler : AuthorizationHandler<FamilyOrRolesAuthorizationRequirement>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, FamilyOrRolesAuthorizationRequirement requirement)
        {
            var user = new AuthUser(context.User.Claims);
            var filterContext = context.Resource as AuthorizationFilterContext;
            var routeValues = filterContext.RouteData.Values;

            if (requirement.Roles.Contains(user.Role))
            {
                context.Succeed(requirement);
            }
            else if (String.IsNullOrWhiteSpace(user.FamilyId))
            {
                throw new InvalidOperationException("Authenticated used does not have a claim for 'FamilyId'.");
            }
            else if (!routeValues.ContainsKey("familyId"))
            {
                throw new InvalidOperationException("Route values does not contain a key for 'familyId'.");
            }
            else if (String.Equals(routeValues["familyId"], user.FamilyId))
            {
                context.Succeed(requirement);
            }

            return Task.CompletedTask;
        }
    }
}
