namespace Daycare.Controllers.Authentication
{
    public class AuthorizationRoles
    {
        public const string Admin = "admin";
        public const string Teacher = "teacher";
        public const string User = "user";
    }
}