﻿using System;
using System.Collections.Generic;
using System.Security.Claims;

namespace Daycare.Controllers.Authentication
{
    public class AuthUser
    {
        private const string CLAIM_TYPE_FAMILY_ID = "FamilyId";

        public ClaimsPrincipal Principal { get; private set; }

        public AuthUser(string id, string familyId, string displayName, string email, string role)
            : this(new Claim[]
            {
                new Claim(ClaimTypes.NameIdentifier, id),
                new Claim(ClaimTypes.Name, displayName),
                new Claim(ClaimTypes.Email, email),
                new Claim(ClaimTypes.Role, role ?? AuthorizationRoles.User),
                new Claim(CLAIM_TYPE_FAMILY_ID, familyId ?? String.Empty)
            })
        {
        }

        public AuthUser(IEnumerable<Claim> claims)
        {
            var identity = new ClaimsIdentity(claims, "OAuth");
            Principal = new ClaimsPrincipal(identity);
        }

        private string GetClaimValue(string claimType)
        {
            var claim = Principal.FindFirst(claimType);
            return claim?.Value;
        }

        public string Id
        {
            get
            {
                return GetClaimValue(ClaimTypes.NameIdentifier);
            }
        }

        public string FamilyId
        {
            get
            {
                return GetClaimValue(CLAIM_TYPE_FAMILY_ID);

            }

        }

        public string DisplayName
        {
            get
            {
                return GetClaimValue(ClaimTypes.Name);
            }
        }

        public string Email
        {
            get
            {
                return GetClaimValue(ClaimTypes.Email);

            }
        }

        public string Role
        {
            get
            {
                return GetClaimValue(ClaimTypes.Role);
            }
        }
    }
}
