﻿using Daycare.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System;
using Daycare.DataModels;
using Daycare.Controllers.ResponseModels;
using Daycare.Controllers.RequestModels;
using Daycare.Controllers.Authentication;

namespace Daycare.Controllers
{
    [Authorize]
    [AutoValidateAntiforgeryToken]
    [Route("api/[controller]")]
    public class FamiliesController : Controller
    {
        private readonly IFamilyDataService _familyService;
        private readonly IPersonDataService _personService;
        private readonly IClassDataService _classService;

        public FamiliesController(IFamilyDataService familyService, IPersonDataService personService, IClassDataService classService)
        {
            _familyService = familyService;
            _personService = personService;
            _classService = classService;
        }

        [HttpGet("{id}")]
        public async Task<FamilyResponseModel> GetFamily(string id, bool expandMembers = false, bool expandClasses = false)
        {
            var familyTask = _familyService.GetFamilyAsync(id);
            var membersTask = expandMembers ? GetFamilyMembersAsync(id, expandClasses) : Task.FromResult(Enumerable.Empty<PersonResponseModel>());
            await Task.WhenAll(familyTask, membersTask);

            var responseModel = FamilyResponseModel.FromDataModel(familyTask.Result);
            if (expandMembers)
            {
                responseModel.Children = membersTask.Result.Where(p => p.Role == PersonRoles.Child);
                responseModel.Parents = membersTask.Result.Where(p => p.Role == PersonRoles.Parent);
            }

            return responseModel;
        }

        [HttpGet]
        public async Task<IEnumerable<FamilyResponseModel>> GetFamilies(bool expandMembers = false)
        {
            var user = new AuthUser(this.User.Claims);
            var familiesTask = _familyService.GetFamiliesAsync();
            var membersTask = Task.FromResult(Enumerable.Empty<Person>());

            if (expandMembers)
            {
                membersTask = _personService.GetPersonsAsync(p => p.Role == PersonRoles.Parent || p.Role == PersonRoles.Child);
            }

            await Task.WhenAll(familiesTask, membersTask);

            var responseModel = familiesTask.Result.Select(family =>
            {
                var familyModel = FamilyResponseModel.FromDataModel(family);
                if (expandMembers)
                {
                    familyModel.LoadMembers(membersTask.Result, user);
                }

                return familyModel;
            });

            return responseModel;
        }

        [HttpDelete("{familyId}")]
        [AuthorizeRoles(AuthorizationRoles.Admin, AuthorizationRoles.Teacher)]
        public async Task<IActionResult> DeleteFamily(string familyId)
        {
            await _familyService.DeleteFamilyAsync(familyId);
            return Ok();
        }

        [HttpPut("{familyId}")]
        [Authorize(Policy = AuthorizationPolicies.FamilyOrAdmin)]
        public async Task<IActionResult> UpdateFamily([FromBody] FamilyUpdateRequestModel requestModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var updatedFamily = await _familyService.UpdateFamilyAsync(requestModel.ToDataModel());
            return Ok(FamilyResponseModel.FromDataModel(updatedFamily));
        }

        [HttpPost]
        [AuthorizeRoles(AuthorizationRoles.Admin, AuthorizationRoles.Teacher)]
        public async Task<IActionResult> InsertFamily([FromBody] FamilyInsertRequestModel requestModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var newFamily = await _familyService.InsertFamilyAsync(requestModel.ToDataModel());
            var responseModel = FamilyResponseModel.FromDataModel(newFamily);
            responseModel.Children = Enumerable.Empty<PersonResponseModel>();
            responseModel.Parents = Enumerable.Empty<PersonResponseModel>();
            return Ok(responseModel);
        }

        [HttpPost("{familyId}/children")]
        [Authorize(Policy = AuthorizationPolicies.FamilyOrAdmin)]
        public async Task<IActionResult> AddChild(string familyId, [FromBody] ChildInsertRequestModel requestModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = new AuthUser(this.User.Claims);
            var person = requestModel.ToDataModel();
            person.FamilyId = familyId;
            person.Role = PersonRoles.Child;

            var newPerson = await _personService.AddPersonAsync(person);
            var responseModel = PersonResponseModel.FromDataModel(person, user);
            responseModel.Classes = Enumerable.Empty<ClassResponseModel>();
            return Ok(responseModel);
        }

        [HttpDelete("{familyId}/children/{childId}")]
        [Authorize(Policy = AuthorizationPolicies.FamilyOrAdmin)]
        public async Task<IActionResult> DeleteChild(string familyId, string childId)
        {
            await DeleteFamilyMemberAsync(childId, familyId);
            return Ok();
        }

        [HttpPut("{familyId}/children/{childId}")]
        [Authorize(Policy = AuthorizationPolicies.FamilyOrAdmin)]
        public async Task<IActionResult> UpdateChild(string familyId, [FromBody] ChildUpdateRequestModel requestModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = new AuthUser(this.User.Claims);
            var personModel = requestModel.ToDataModel();
            var updatedPerson = await UpdateFamilyMemberAsync(personModel, familyId);
            return Ok(PersonResponseModel.FromDataModel(updatedPerson, user));
        }

        [HttpPost("{familyId}/parents")]
        [Authorize(Policy = AuthorizationPolicies.FamilyOrAdmin)]
        public async Task<IActionResult> AddParent(string familyId, [FromBody] ParentInsertRequestModel requestModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var person = requestModel.ToDataModel();
            person.FamilyId = familyId;
            person.Role = PersonRoles.Parent;

            var user = new AuthUser(this.User.Claims);
            var newPerson = await _personService.AddPersonAsync(person);
            var responseModel = PersonResponseModel.FromDataModel(person, user);
            responseModel.Classes = Enumerable.Empty<ClassResponseModel>();
            return Ok(responseModel);
        }

        [HttpDelete("{familyId}/parents/{parentId}")]
        [Authorize(Policy = AuthorizationPolicies.FamilyOrAdmin)]
        public async Task<IActionResult> DeleteParent(string familyId, string parentId)
        {
            await DeleteFamilyMemberAsync(parentId, familyId);
            return Ok();
        }

        [HttpPut("{familyId}/parents/{parentId}")]
        [Authorize(Policy = AuthorizationPolicies.FamilyOrAdmin)]
        public async Task<IActionResult> UpdateParent(string familyId, [FromBody] ParentUpdateRequestModel requestModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = new AuthUser(this.User.Claims);
            var personModel = requestModel.ToDataModel();
            var updatedPerson = await UpdateFamilyMemberAsync(personModel, familyId);
            return Ok(PersonResponseModel.FromDataModel(updatedPerson, user));
        }

        private async Task<IEnumerable<PersonResponseModel>> GetFamilyMembersAsync(string familyId, bool expandClasses)
        {
            var user = new AuthUser(this.User.Claims);
            var persons = await _personService.GetPersonsAsync(p => p.FamilyId == familyId);
            var models = persons.Select(p => PersonResponseModel.FromDataModel(p, user)).ToList();

            if (expandClasses)
            {
                var classIds = models.SelectMany(p => p.ClassIds);
                var classes = await _classService.GetClassesAsync(c => classIds.Contains(c.Id));
                var classesMap = classes.ToDictionary(c => c.Id);

                foreach (var responseModel in models)
                {
                    responseModel.Classes = responseModel.ClassIds
                        .Where(classId => classesMap.ContainsKey(classId))
                        .Select(classId => ClassResponseModel.FromDataModel(classesMap[classId]));
                }
            }

            return models;
        }

        private async Task DeleteFamilyMemberAsync(string personId, string expectedFamilyId)
        {
            var person = (await _personService.GetPersonsAsync(p => p.Id == personId && p.FamilyId == expectedFamilyId)).FirstOrDefault();

            if (person != null)
            {
                await _personService.DeletePersonAsync(personId);
            }
        }

        private async Task<Person> UpdateFamilyMemberAsync(Person updatedPerson, string expectedFamilyId)
        {
            var person = (await _personService.GetPersonsAsync(p => p.Id == updatedPerson.Id && p.FamilyId == expectedFamilyId)).FirstOrDefault();

            if (person == null)
            {
                throw new InvalidOperationException($"Unable to update person with id '{updatedPerson.Id}', family id provided '{expectedFamilyId}' does not matched the value stored for user.");
            }

            return await _personService.UpdatePersonAsync(updatedPerson);
        }
    }
}
